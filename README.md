# 1. What does the clinical capacity management tool do?

## Background

Clinical intensive care beds that e.g. include the necessary respiratory equipment to treat corona patients are limited. Hence, when the corona pandemic started to spread, one of the most relevant threats was that intensive care capacities in hospitals would not be sufficient in some areas to treat all patients. Not only are intensive care capacities limited but, as far as we know, there is no central coordination of patient reallocation that takes forecasts of futures capacity utilization, e.g. the occurence of new patients as well as discharges of patients, into account. At the moment, IVENA allows to track free beds and allocates new patients based on the current availability. However, most regions still rely on an inefficient phone call-based allocation of new patients.

## Forward-looking clinical capacity management

With our tool, a crisis management group, a state's Ministry of Health or some organisation assigned to organize the allocation of patients to hospitals such as IVENA can decide where to best send each patient.

Based on daily data on hospital capacity utilization (provided by each hospital to DIVI), e.g. how many beds are available in total, how many of which are already occupied etc. as well as daily number of new covid cases per district (provided by Germany's Robert Koch Institute - RKI), a daily forecast of expected new cases of covid and non-covid patients as well as people discharged from hospitals per district is calculated. Using these forecasts, the allocation of new patients to each district is calculated. Hence, among other things, the result would be a reallocation of patients to other districts for the specified period of time.   

## What can it do?

![Hospital Bed utilization with and without capacity management](./backend/configurations/results_30days_comparison.gif "Comparison 30 days")


# 2. How to use the tool

## Get started by setting up the system ##

To install our clinical capacity management tool, one may use either `pipenv` or use the option of a `Remote Container` provided by Visual Studio Code. Both options presume that you already installed Docker on your system. 
First switch to the backend directory:

``` sh
cd backend
```

### Using pipenv
``` sh
pip install --user pipenv
pipenv install
pip install -e .
```

In case you pulled and need to update packages based on the pipfile.lock just enter
``` sh
pipenv sync
```

For a docker setup:

``` sh
docker-compose up
```

### Using Visual Studio Code's Remote Container

1. Install [Visual Studio Code](https://code.visualstudio.com) or [Visual Studio Code](https://code.visualstudio.com/insiders)
2. Install the VS Code extension Remote - Containers: `ms-vscode-remote.remote-containers`
3. Start VS Code
4. Run the **Remote-Contrainers: Open Folder in Container...** command and select the local folder.


## Fill database with all relevant data

To get started for the first time you need to import some data into the database. You can do this with the following commands:

- German interdisciplinary association for intensive and emergency medicine (DIVI): `enabler divi poll-archive`
- Locations: `enabler germany fill-db`
- Robert Koch Institut (RKI): `enabler rki fill-db`

Afterwards you can also run the API locally by entering
```
enabler api run
```

## Gemeindeschlüssel, Kreis ID

There are various regional identification methods and some online resources confuse them:

* Amtlicher Gemeindeschlüssel (ags): https://de.wikipedia.org/wiki/Amtlicher_Gemeindeschl%C3%BCssel
* Kreis ID / Kreisschlüssel: Normally the first 5 numbers of the ags (see https://de.wikipedia.org/wiki/Amtlicher_Gemeindeschl%C3%BCssel#Aufbau).
* Regionalschlüssel: This is the "modern" version of the ags and is more komplex (https://de.wikipedia.org/wiki/Amtlicher_Gemeindeschl%C3%BCssel#Regionalschl%C3%BCssel)
