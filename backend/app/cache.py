from contextlib import suppress
from pathlib import Path
from typing import Union

import cachetools


class FileCache(cachetools.LRUCache):
    cache_folder: Path

    def __init__(self, maxsize: int, cache_folder: Union[Path, str], key=None):
        super().__init__(maxsize=maxsize)
        self.cache_folder = Path(cache_folder)
        self.cache_folder.mkdir(parents=True, exist_ok=True)

    def __getitem__(self, args):
        filename = self.build_path(args)
        try:
            with open(filename) as f:
                return f.read()
        except FileNotFoundError:
            raise KeyError(f"File {filename} not found.")

    def __setitem__(self, args, value):
        filename = self.build_path(args)
        with open(filename, "w") as f:
            f.write(value)

    def __delitem__(self, args):
        filename = self.build_path(args)
        with suppress(FileNotFoundError):
            filename.unlink()

    def build_path(self, args) -> Path:
        """Build a cache file name given function arguments."""
        filename = "_".join(str(a) for a in args)
        return self.cache_folder / filename
