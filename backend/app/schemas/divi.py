import datetime
from contextlib import suppress
from typing import Dict, Optional

from pydantic import BaseModel, Field, root_validator, validator


class ArchiveRecord(BaseModel):
    district_id: Optional[str] = Field(None, min_length=5, max_length=5)
    state_id: Optional[str] = Field(None, min_length=2, max_length=2)
    report_area_count: Optional[int] = None
    current_covid_case_count: Optional[int] = None
    current_covid_case_ventilated_count: Optional[int] = None
    site_count: Optional[int] = None
    bed_unoccupied_count: Optional[int] = None
    bed_occupied_count: Optional[int] = None
    updated_at: Optional[datetime.datetime] = None
    date: datetime.date

    _column_rename_map: Dict[str, str] = {
        "bundesland": "state_id",
        "gemeindeschluessel": "district_id",
        "kreis": "district_id",
        "anzahl_meldebereiche": "report_area_count",
        "faelle_covid_aktuell": "current_covid_case_count",
        "faelle_covid_aktuell_beatmet": "current_covid_case_ventilated_count",
        "anzahl_standorte": "site_count",
        "betten_frei": "bed_unoccupied_count",
        "betten_belegt": "bed_occupied_count",
        "daten_stand": "updated_at",
    }

    @root_validator(pre=True)
    def convert_fields(cls, values):
        if isinstance(values, dict):
            for old_key in set(values).intersection(cls._column_rename_map):
                new_key = cls._column_rename_map[old_key]
                with suppress(KeyError):
                    values.setdefault(new_key, values.pop(old_key))
        return values

    @validator("bed_unoccupied_count", "bed_occupied_count", pre=True)
    def convert_float_to_int(cls, value):
        """Sometimes we need to interpret float strings (for example "32.0") as integers as well."""
        with suppress(Exception):
            return int(value)

        with suppress(Exception):
            return int(float(value))

        return value

    @validator("state_id", always=True)
    def fill_state_id(cls, value, values):
        if value is None:
            district_id = values.get("district_id")
            if district_id:
                return district_id[:2]
        return value

    class Config:
        orm_mode = True


class BedStatusUpdate(BaseModel):
    """Model for representing bed status of a intensive care unit."""

    low_care: Optional[str]
    high_care: Optional[str]
    ecmo: Optional[str]
    published: datetime.datetime


class Location(BaseModel):
    latitude: float
    longitude: float
