import datetime
import enum

from pydantic import BaseModel, Field, validator
from pytz import timezone


class CaseType(str, enum.Enum):
    ACTIVE = "ACTIVE"
    DEATH = "DEATH"
    RECOVERY = "RECOVERED"


class Gender(str, enum.Enum):
    MALE = "M"
    FEMALE = "F"
    UNKNOWN = ""


class RkiCovid19Case(BaseModel):
    """Schema for a RKI Covid 19 Case"""

    object_id: int
    case_type: CaseType
    reported_at: datetime.datetime
    referenced_at: datetime.datetime
    updated_at: datetime.datetime
    district_id: str = Field(
        ..., min_length=5, max_length=5, description="Kreisschlüssel"
    )
    state_id: str = Field(None, min_length=2, max_length=2, description="Bundeland")
    gender: Gender
    age_group: str
    reference_date: datetime.date = Field(None)

    class Config:
        orm_mode = True

    @validator("state_id", always=True)
    def fill_state_id(cls, value, values):
        if value is None:
            return values["district_id"][:2]
        return value

    @validator("reference_date", always=True)
    def fill_reference_date(cls, value, values):
        return (
            values["referenced_at"]
            .astimezone(timezone("UTC"))
            .astimezone(timezone("Europe/Berlin"))
            .date()
        )


class DailyCaseSummary(BaseModel):
    date: datetime.date = Field(
        ..., descriptions="Date of reference (start of sickness or report date)"
    )
    state_id: str = Field(..., min_length=2, max_length=2)
    district_id: str = Field(..., min_length=5, max_length=5)
    cases: int = Field(..., description="Number of cases for this date.")
