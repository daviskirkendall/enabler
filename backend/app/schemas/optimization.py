"""Schemas used in the optimization."""

import datetime
from typing import Any, Dict, List, Optional

from pydantic import BaseModel, Field


class Patients(BaseModel):

    category: str
    forecast: List[int]
    avg_healing_time: int
    relocation_possible: bool


class LocationProperties(BaseModel):

    name: str
    longitude: float
    latitude: float
    occupation: int
    capacity: int
    discharge_forecast: List[int]
    new_patients: List[Patients]


class ModelConfiguration(BaseModel):

    forecast_horizon: int = Field(
        ..., description="Number of timesteps in each forecast."
    )
    locations: List[LocationProperties] = Field(
        ..., description="List of locations to process."
    )
    meta: Dict[str, Any] = Field(
        {}, description="Additional unstructured information about configuration."
    )


class Allocation(BaseModel):
    from_location: str
    to_location: str
    category: str
    time: int
    result: int


class ModelResults(BaseModel):
    objective: float
    allocations: List[Allocation]
    occupation: Dict[str, List[int]]


class JobInputSchema(BaseModel):
    configuration: ModelConfiguration


class CreatedJob(BaseModel):
    id: int = Field(..., description="Id of triggered job")


class JobSchema(BaseModel):
    id: int = Field(..., description="Id of job")
    created_at: datetime.datetime = Field(..., description="Created at this date")
    updated_at: datetime.datetime = Field(..., description="Updated last at this date")
    started_at: Optional[datetime.datetime] = Field(
        None, description="Started at this date"
    )
    finished_at: Optional[datetime.datetime] = Field(
        None, description="Finished at this date"
    )
    status: str = Field("Status of optimization.")
