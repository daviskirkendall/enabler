from typing import Optional, Tuple

from pydantic import BaseModel, Field, validator


class District(BaseModel):
    """Schema for german district (german: Kreis)."""

    district_id: str = Field(..., description="District id (Kreisschlüssel).")
    name: str = Field(..., description="Name of district")
    state_id: str = Field(None, description="State id (Bundesland).")

    @validator("state_id", always=True)
    def fill_state_id(cls, value, values):
        if value is None:
            return values["district_id"][:2]
        return value


class AggregatedDistrict(District):
    population: int = Field(..., description="Population of district.")
    area: float = Field(..., description="Total area of district.")
    latitude: float = Field(..., description="Latitude of district center.")
    longitude: float = Field(..., description="Longitude of district center.")

    @property
    def location(self) -> Tuple[float, float]:
        return self.latitude, self.longitude


class Community(BaseModel):
    """Schema for german community definition (german: Gemeinde)."""

    name: str = Field(..., description="Name of district")
    community_id: str = Field(..., description="Community id (Regionalschlüssel)")
    district_id: str = Field(None, description="District id (Kreisschlüssel)")
    state_id: str = Field(None, description="State id (Bundesland")
    population: int = Field(..., description="Population of district.")
    area: int = Field(..., description="Area in square kilometers.")
    latitude: float = Field(..., description="Latitude of district center.")
    longitude: float = Field(..., description="Longitude of district center.")
    zip_code: Optional[str] = Field(None, description="Postleitzahl.")

    @property
    def location(self) -> Tuple[float, float]:
        return self.latitude, self.longitude

    @validator("district_id", always=True)
    def fill_district_id(cls, value, values):
        if value is None:
            return values["community_id"][:5]
        return value

    @validator("state_id", always=True)
    def fill_state_id(cls, value, values):
        if value is None:
            return values["community_id"][:2]
        return value
