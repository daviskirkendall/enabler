import sys
from contextlib import contextmanager
from contextvars import ContextVar
from typing import Optional
from weakref import WeakSet

import sqlalchemy as sa

from app.settings import SETTINGS


class Database:
    """Simple class for managing connections, transactions and database executions.

    Manages seperate connections for each thread.
    """

    def __init__(self, url: str, **kwargs) -> None:
        self._connection_context: ContextVar[
            Optional[sa.engine.Connection]
        ] = ContextVar("database_connection")
        self.engine = sa.create_engine(url, **kwargs)
        self._connections: WeakSet[sa.engine.Connection] = WeakSet()

    def __enter__(self):
        return self

    def __exit__(self):
        self.disconnect()
        self.engine.dispose()

    def connect(self) -> sa.engine.Connection:
        """Return the current connection."""
        conn = self._connection
        if not conn or conn.closed:
            self._connection = conn = self.engine.connect()
        return conn

    def disconnect(self) -> None:
        """Disconnect all connections."""
        for conn in self._connections:
            conn.close()

    @property
    def _connection(self) -> Optional[sa.engine.Connection]:
        conn = self._connection_context.get(None)
        return conn

    @_connection.setter
    def _connection(self, conn) -> None:
        self._connection_context.set(conn)
        self._connections.add(conn)

    @contextmanager
    def transaction(self) -> sa.engine.Transaction:
        """Context manager to start a transaction and roll back on exceptions."""
        conn = self.connect()
        trans = conn.begin_nested()
        try:
            yield conn
        except Exception:
            trans.rollback()
            raise
        else:
            trans.commit()
        finally:
            if not conn.in_transaction():
                conn.close()

    def execute(self, *args, **kwargs):
        """Execute a database query (proxy to the "execute" method of the current connection)."""
        con = self.connect()
        try:
            return con.execute(*args, **kwargs)
        finally:
            if not con.in_transaction():
                con.close()


database = Database(SETTINGS.db_uri)
schema = "pytest" if "pytest" in " ".join(sys.argv) else None
metadata = sa.MetaData(schema=schema)


def init_db():
    metadata.create_all(bind=database.engine)
