from pydantic import BaseSettings, PostgresDsn


class Settings(BaseSettings):
    db_uri: PostgresDsn = "postgres://postgres:postgres@localhost:5432/postgres"

    class Config:
        env_prefix = "enabler_"


SETTINGS = Settings()
