"""Model for matching of hospitals and patients."""

import json
import logging
import pathlib
from collections import defaultdict
from typing import Any, DefaultDict, Dict, Iterable, Iterator, List, Tuple

from geopy import distance
from ortools.sat.python import cp_model

from app.schemas.optimization import (
    Allocation,
    LocationProperties,
    ModelConfiguration,
    ModelResults,
    Patients,
)

logger = logging.getLogger(__name__)

DD = DefaultDict


class MatchingModel:

    configuration: ModelConfiguration
    results: ModelResults

    def __init__(self, configuration, max_distance=None):
        """Initialize matching model."""
        self.max_distance = max_distance
        self.configuration = ModelConfiguration.parse_obj(configuration)
        self.locations = self.configuration.locations
        self.horizon = self.configuration.forecast_horizon
        self.distances = self.calculate_distances()
        self.destinations = self.create_feasible_destinations()
        self.model = cp_model.CpModel()

    def calculate_distances(self):
        """Calculate distances between locations."""
        distances: dict = {}
        for from_location in self.locations:
            distances[from_location.name] = {}
            for to_location in self.locations:
                distances[from_location.name][to_location.name] = round(
                    distance.distance(
                        (from_location.latitude, from_location.longitude),
                        (to_location.latitude, to_location.longitude),
                    ).km
                )
        return distances

    def solve(self):
        """Solve matching model."""
        logging.info("Creating variables.")
        self.create_allocation()
        self.create_occupied()
        self.create_over_occupation()

        logging.info("Adding constraints.")
        self.allocate_patients()
        self.calculate_occupation()
        self.assign_over_occupation()

        logging.info("Adding objective.")
        self.add_objective()
        logging.info("Solving model.")
        self.solver = cp_model.CpSolver()
        self.solver.parameters.log_search_progress = True
        self.solver.parameters.num_search_workers = 6
        self.solver.Solve(self.model)
        logging.info("Getting results.")
        self.get_results()

    def create_feasible_destinations(self):
        destinations: Dict[str, Any] = {}
        for from_location in self.locations:
            destinations[from_location.name] = {}
            for to_location in self.locations:
                if (
                    self.max_distance is not None
                    and self.distances[from_location.name][to_location.name]
                    > self.max_distance
                ):
                    continue
                if self.max_distance == 0 and from_location.name != to_location.name:
                    continue
                destinations[from_location.name][to_location.name] = to_location
        return destinations

    def _iter_patient_destinations(
        self, from_location: LocationProperties
    ) -> Iterator[Tuple[Patients, Iterable[LocationProperties]]]:
        for patients in from_location.new_patients:
            if patients.relocation_possible:
                destinations: Iterable[LocationProperties] = list(
                    self.destinations[from_location.name].values()
                )
            else:
                # non-relocatable patients only destination is their origin
                destinations = [from_location]
            yield patients, destinations

    def create_allocation(self):
        """Create allocation[from_location][to_location][patient_category][time] variables."""
        dd = defaultdict
        self.allocation: DD[str, DD[str, Dict[str, List[Any]]]] = dd(lambda: dd(dict))
        for from_location in self.locations:
            for patients, to_locations in self._iter_patient_destinations(
                from_location
            ):
                for to_location in to_locations:
                    self.allocation[from_location.name][to_location.name][
                        patients.category
                    ] = [
                        self.model.NewIntVar(
                            0,
                            patients.forecast[t],
                            f"allocation_{from_location.name}_{to_location.name}_{patients.category}_{t}",
                        )
                        for t in range(self.horizon)
                    ]

    def create_occupied(self):
        """Create occupied[location][time] variables."""
        self.occupied = {
            location.name: [
                self.model.NewIntVar(
                    0, 5 * location.capacity, f"occupied_{location.name}_{t}"
                )
                for t in range(self.horizon)
            ]
            for location in self.locations
        }

    def create_over_occupation(self):
        """Create over occupation of patients."""
        self.over_occupation = {
            location.name: [
                self.model.NewIntVar(
                    0, 4 * location.capacity, f"over_occupation_{location.name}_{t}"
                )
                for t in range(self.horizon)
            ]
            for location in self.locations
        }

    def allocate_patients(self):
        """Ensure that all patients get allocated."""
        for from_location in self.locations:
            for patients, to_locations in self._iter_patient_destinations(
                from_location
            ):
                for t in range(self.horizon):
                    self.model.Add(
                        sum(
                            self.allocation[from_location.name][to_location.name][
                                patients.category
                            ][t]
                            for to_location in to_locations
                        )
                        == patients.forecast[t]
                    )

    def calculate_occupation(self):
        """Calculate the projected occupation for each location."""
        new_patients_map: DefaultDict[
            str, List[Tuple[LocationProperties, Patients]]
        ] = defaultdict(list)
        for from_location in self.locations:
            for patients, to_locations in self._iter_patient_destinations(
                from_location
            ):
                for to_location in to_locations:
                    new_patients_map[to_location.name].append((from_location, patients))

        for to_location in self.locations:
            source_patients = new_patients_map[to_location.name]
            for t in range(self.horizon):
                self.model.Add(
                    # newly allocated patients
                    sum(
                        self.allocation[from_location.name][to_location.name][
                            patients.category
                        ][t]
                        for from_location, patients in source_patients
                    )
                    # patients that are already there
                    + (
                        to_location.occupation
                        if t == 0
                        else self.occupied[to_location.name][t - 1]
                    )
                    # patients that are healed
                    - sum(
                        self.allocation[from_location.name][to_location.name][
                            patients.category
                        ][t - patients.avg_healing_time]
                        for from_location, patients in source_patients
                        if t - patients.avg_healing_time >= 0
                    )
                    - to_location.discharge_forecast[t]
                    == self.occupied[to_location.name][t]
                )

    def assign_over_occupation(self):
        """Assign over occupation variable."""
        for location in self.locations:
            for t in range(self.horizon):
                self.model.Add(
                    self.occupied[location.name][t]
                    <= location.capacity + self.over_occupation[location.name][t]
                )

    def add_objective(self):
        """Add an objective.

        Minimize the sum of distances for allocated patients as well as number of reallocations.
        Reallocations in the short term bear higher costs (100)
        compared to reallocations in the future (100-t).


        """
        objective = sum(
            (100 - t)
            * (
                self.distances[from_location.name][to_location.name]
                + int(from_location.name != to_location.name)
            )
            * self.allocation[from_location.name][to_location.name][patients.category][
                t
            ]
            for from_location in self.locations
            for patients, to_locations in self._iter_patient_destinations(from_location)
            for to_location in to_locations
            for t in range(self.horizon)
        )
        over_allocation = sum(
            1_000_000 * self.over_occupation[location.name][t]
            for location in self.locations
            for t in range(self.horizon)
        )
        self.model.Minimize(objective + over_allocation)

    def get_allocations(self) -> List[Allocation]:
        """Get allocation results."""
        allocations = []
        reallocations_from = {}
        for from_location in self.locations:
            reallocations_from[from_location.name] = 0
            for to_location_name in self.destinations[from_location.name]:
                for patients in from_location.new_patients:
                    if (
                        not patients.relocation_possible
                        and from_location.name != to_location_name
                    ):
                        continue
                    for t in range(self.horizon):
                        allocation: Allocation = Allocation(
                            from_location=from_location.name,
                            to_location=to_location_name,
                            category=patients.category,
                            time=t,
                            result=self.solver.Value(
                                self.allocation[from_location.name][to_location_name][
                                    patients.category
                                ][t]
                            ),
                        )
                        if allocation.result != 0:
                            allocations.append(allocation)
                            if from_location.name != to_location_name:
                                reallocations_from[
                                    from_location.name
                                ] += allocation.result
        logging.info(
            f"A total of {sum(reallocations_from.values())} patients was reallocated."
        )
        for name, number in reallocations_from.items():
            if number > 0:
                logging.info(f"{number} covid patients from {name} got reallocated.")
        return allocations

    def get_occupation(self) -> Dict[str, List[int]]:
        """Get occupation results."""
        occupation: Dict[str, List[int]] = {}
        for location in self.locations:
            occupation[location.name] = []
            for t in range(self.horizon):
                occupation[location.name].append(
                    self.solver.Value(self.occupied[location.name][t])
                )
        return occupation

    def get_results(self):
        """Get the allocation results."""
        if self.solver.StatusName() == "INFEASIBLE":
            raise ValueError("Model could not be solved.")

        self.results = ModelResults(
            objective=self.solver.ObjectiveValue(),
            allocations=self.get_allocations(),
            occupation=self.get_occupation(),
        )


def run_matching(config_filename, max_distance):
    path = get_default_path(config_filename)
    configuration = read_config(path)
    model = MatchingModel(configuration, max_distance)
    model.solve()
    filename = path.parent / f"results_{path.stem}_{max_distance}.json"
    logger.info(f"Writing results to {filename}.")
    with open(filename, "w") as f:
        json.dump(
            {
                "results": model.results.dict(),
                "configuration": model.configuration.dict(),
                "max_distance": max_distance,
            },
            f,
            indent=4,
            ensure_ascii=False,
        )


def get_default_path(name: str):
    """Return the path to default files."""
    default_path = pathlib.Path(__file__).parent.parent.parent / name
    return default_path


def read_config(filename: str):
    """Read in yaml or json file."""
    with open(filename, "r") as f:
        data = f.read()
        return json.loads(data)
