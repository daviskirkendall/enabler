import logging
from typing import Optional, Union

import sqlalchemy as sa

from app.db import database
from app.models.optimization import optimizations_table
from app.optimization.matching import MatchingModel
from app.schemas.optimization import ModelConfiguration
from app.tasks import Job, RetrySpec, queue

logger = logging.getLogger(__name__)


def create_new_optimization(configuration: Union[dict, ModelConfiguration]) -> int:
    conf_dict = ModelConfiguration.parse_obj(configuration).dict()
    optimization_id = database.execute(
        optimizations_table.insert()
        .values(configuration=conf_dict)
        .returning(optimizations_table.c.id)
    ).first()["id"]
    return optimization_id


def store_optimization_finalization(id, result=None, error=None):
    # Store results in database
    status = "FAILURE" if error is not None else "SUCCESS"
    database.execute(
        optimizations_table.update()
        .where(optimizations_table.c.id == id)
        .values(finished_at=sa.func.now(), result=result, error=error, status=status)
    )


def store_optimization_start(id: int, job_queue_id: int):
    database.execute(
        optimizations_table.update()
        .where(optimizations_table.c.id == id)
        .values(job_queue_id=job_queue_id, started_at=sa.func.now(), status="STARTED")
    )


@queue.task(pass_job=True, retry=RetrySpec(max_retries=10, delay_multiplier=1))
def run_optimization(job: Optional[Job], id: int,) -> bool:
    try:
        job_queue_id = job.id if job is not None else None
        store_optimization_start(id=id, job_queue_id=job_queue_id)

        # fetch configuration
        configuration_dict = database.execute(
            sa.select([optimizations_table.c.configuration]).where(
                optimizations_table.c.id == id
            )
        ).scalar()
        configuration = ModelConfiguration.parse_obj(configuration_dict)

        # Run optimization
        model = MatchingModel(configuration)
        model.solve()

        # Store results in database
        store_optimization_finalization(id, result=model.results.dict())
        return True
    except sa.exc.OperationalError:
        # disconnects should trigger retries
        raise  # will trigger a retry
    except Exception as e:
        logger.warning("Optimization failed", exc_info=True)
        store_optimization_finalization(
            id, error={"type": type(e).__name__, "msg": str(e)}
        )
        return False
