"""RKI data via api access.

Explore the dataset that is used as a source here:

https://npgeo-corona-npgeo-de.hub.arcgis.com/datasets/dd4580c810204019a7b8eb3e0b329dd6_0
"""
import datetime
import logging
from typing import Iterable, Iterator, List

import pandas as pd
import requests
import sqlalchemy as sa
from pydantic.tools import parse_obj_as

from app.db import database
from app.models.rki import rki_covid19_cases_table
from app.schemas import rki as schemas
from app.schemas.rki import DailyCaseSummary

logger = logging.getLogger(__name__)


BASE_API_URL = "https://services7.arcgis.com/mOBPykOjAyBO2ZKk/arcgis/rest/services/RKI_COVID19/FeatureServer/0/query"
DEFAULT_API_PARAMS = {
    "f": "json",
    "where": "1=1",
    "outFields": "*",
    "orderByFields": "Meldedatum",
}


def _request_api(params: dict = None):
    request_params = {**DEFAULT_API_PARAMS, **(params if params is not None else {})}
    response = requests.get(
        BASE_API_URL,
        headers={"Accept": "application/json"},
        params=request_params,
        timeout=60,
    )
    response.raise_for_status()
    return response


def _get_number_of_records() -> int:
    return _request_api({"returnCountOnly": "true"}).json()["count"]


def _request_data(offset: int, limit: int) -> requests.Response:
    params = {}
    if offset:
        params["resultOffset"] = str(int(offset))
    if limit:
        params["resultRecordCount"] = str(int(limit))
    return _request_api(params)


def iter_fetch_data(limit=None, offset=0) -> Iterator[dict]:
    """Iteratively fetch rki data from api."""
    page_size = 4000
    total = 0
    expected_total = _get_number_of_records()
    logger.info(f"Total number of records in dataset: {expected_total}")
    for request_offset in range(offset, expected_total, page_size):
        response = _request_data(offset=request_offset, limit=page_size)
        response_json = response.json()
        features = response_json["features"]
        for feature in features:
            total += 1
            if limit is not None and total > limit:
                return
            entry = feature["attributes"]
            yield entry
        logger.info(
            f"fetched {len(features)} entries starting at entry {request_offset}, total of {total} fetched."
        )


def iter_parse_entry(entry: dict) -> Iterator[schemas.RkiCovid19Case]:
    """Iterate of all cases held in a single api entry."""
    n_cases = entry["AnzahlFall"]
    n_deaths = entry["AnzahlTodesfall"]
    n_recoveries = entry["AnzahlGenesen"]
    n_active = n_cases - n_deaths - n_recoveries
    if n_active < 0:
        logger.warning(f"Deaths/Recoveries without cases found: {entry}")
    case_types = (
        [schemas.CaseType.DEATH] * n_deaths
        + [schemas.CaseType.RECOVERY] * n_recoveries
        + [schemas.CaseType.ACTIVE] * n_active
    )
    for case_type in case_types:
        case = schemas.RkiCovid19Case(
            object_id=entry["ObjectId"],
            case_type=case_type,
            reported_at=datetime.datetime.utcfromtimestamp(entry["Meldedatum"] / 1000),
            referenced_at=datetime.datetime.utcfromtimestamp(entry["Refdatum"] / 1000),
            updated_at=datetime.datetime.strptime(
                entry["Datenstand"], "%d.%m.%Y, %H:%M Uhr"
            ),
            district_id=entry["IdLandkreis"],
            gender={
                "M": schemas.Gender.MALE,
                "W": schemas.Gender.FEMALE,
                "unbekannt": schemas.Gender.UNKNOWN,
            }[entry["Geschlecht"]],
            age_group=entry["Altersgruppe"],
        )
        yield case


def iter_parse_entries(entries: Iterable[dict]) -> Iterator[schemas.RkiCovid19Case]:
    """Convert api entries to cases."""
    for entry in entries:
        yield from iter_parse_entry(entry)


def build_dataframe(limit=None, offset=0) -> pd.DataFrame:
    """Fetch and interpret cases from api and convert all cases to a single dataframe."""
    return pd.DataFrame(iter_parse_entries(iter_fetch_data(limit=limit, offset=offset)))


def fill_db(limit=None, offset=0) -> None:
    logger.info("Fetching data from api.")
    cases = [
        case.dict()
        for case in iter_parse_entries(iter_fetch_data(limit=limit, offset=offset))
    ]
    logger.info("Storing data in db.")
    with database.transaction():
        database.execute(rki_covid19_cases_table.delete())
        database.execute(rki_covid19_cases_table.insert(), cases)
    logger.info("Stored data in db.")


def daily_cases_from_db(
    district_id: str = None, state_id: str = None
) -> List[DailyCaseSummary]:
    table = rki_covid19_cases_table
    query = sa.select(
        [
            table.c.reference_date.label("date"),
            table.c.state_id,
            table.c.district_id,
            sa.func.count().label("cases"),
        ]
    ).group_by(table.c.reference_date, table.c.state_id, table.c.district_id)
    if district_id is not None:
        query = query.where(table.c.district_id == district_id)
    if state_id is not None:
        query = query.where(table.c.state_id == state_id)

    records = database.execute(query).fetchall()
    return parse_obj_as(List[DailyCaseSummary], records)
