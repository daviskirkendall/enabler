import csv
import datetime
import io
import logging
from pathlib import Path
from typing import Dict, Iterator, List, Optional

import cachetools
import pandas as pd
import requests
from pydantic import parse_obj_as
from pytz import timezone

from app.cache import FileCache
from app.db import database
from app.models.divi import divi_archive_table
from app.schemas import divi as schemas

logger = logging.getLogger(__name__)

CSV_CACHE_FOLDER = Path(".cache/divi/csv")
API_URL = "https://www.intensivregister.de/api/public/intensivregister"
TZ_GERMANY = timezone("Europe/Berlin")


class CsvCache(FileCache):
    def build_path(self, args) -> Path:
        date: datetime.date = args[0]
        return self.cache_folder / (date.isoformat() + ".csv")


def build_divi_csv_archive_url(time: datetime.datetime, attempt):
    url = f"https://www.divi.de/divi-intensivregister-tagesreport-archiv-csv/divi-intensivregister-{time.year:04}-{time.month:02}-{time.day:02}-{time.hour:02}-{time.minute:02}"
    if attempt == 0:
        return url
    else:
        return f"{url}-2"


class DiviArchivePullError(ValueError):
    """Raised when pulling an archive csv fails."""

    def __init__(self, *args, response=None):
        super().__init__(*args)
        self.response = response


def germany_now():
    """Get the current datetime in germany germany"""
    return (
        pd.Timestamp.utcnow()
        .tz_convert("Europe/Berlin")
        .to_pydatetime()
        .replace(tzinfo=None)
    )


csv_cache = CsvCache(maxsize=1000, cache_folder=CSV_CACHE_FOLDER)


@cachetools.cached(cache=csv_cache)
def pull_divi_archive_csv_by_date(date: datetime.date) -> str:
    """Use a date and attempt to download a csv file for that date."""
    # old divi data was uploaded at 9 so we try that if 12 fails.
    attempted_hours = [12, 9]
    error: ValueError = ValueError(
        f"Unknown Error pulling archive data for date {date}"
    )
    csv_str: Optional[str] = None
    url = "unknown"
    for hour in attempted_hours:
        csv_datetime = datetime.datetime.combine(date, datetime.time(hour, 15))
        is_in_future = csv_datetime > germany_now()
        for attempt in range(2):
            try:
                if is_in_future:
                    raise DiviArchivePullError(
                        f"Datetime given ({csv_datetime}) is in the future"
                    )
                url = build_divi_csv_archive_url(csv_datetime, attempt)
                response = requests.get(url, timeout=10)

                if response.status_code == 404:
                    raise DiviArchivePullError(
                        f"No file found at {url}", response=response
                    )
                if response.status_code != 200:
                    raise ValueError(
                        "Something went wrong while requesting the divi website."
                    )
                if '/download"' in response.text:
                    csv_url = url + "/download"
                    csv_response = requests.get(csv_url, timeout=10)
                else:
                    raise DiviArchivePullError(
                        f"No file found at {url}", response=response
                    )
                if (
                    csv_response.status_code == 404
                    or "text/html" in csv_response.headers.get("Content-Type", "")
                ):
                    raise DiviArchivePullError(
                        f"No file found at {url}", response=response
                    )
                elif csv_response.status_code == 200:
                    csv_str = csv_response.text
                    break
                raise ValueError(
                    f"Failed to fetch csv from {url}, status code {response.status_code}: {response.text}"
                )
            except DiviArchivePullError as e:
                error = e

    if csv_str is not None:
        print(f"Fetched csv file from: {url}")
        return csv_str
    raise error


def iter_parse_divi_archive_csv(
    csv_str: str, date=datetime.date
) -> Iterator[schemas.ArchiveRecord]:
    # some of the files have broken line endings, so we fix them here.
    csv_str = csv_str.replace("\r\r", "\n")
    reader = csv.DictReader(io.StringIO(csv_str))
    for row in reader:
        if "daten_stand" in row:
            row["daten_stand"] = datetime.datetime.fromisoformat(row["daten_stand"]).astimezone(TZ_GERMANY)  # type: ignore
        record = schemas.ArchiveRecord(**row, date=date)
        yield record


def iter_divi_archive_dates(start: datetime.date = None, end: datetime.date = None):
    if end is None:
        now = germany_now()
        end = now.date()
        if now.hour < 13:
            # use yesterday if before the todays cutoff time
            end = end - datetime.timedelta(days=1)

    if start is None:
        start = datetime.date(2020, 4, 24)

    date = start
    while date <= end:
        yield date
        date += datetime.timedelta(days=1)


def iter_divi_archive_csv(
    start: datetime.date = None, end: datetime.date = None
) -> Iterator[schemas.ArchiveRecord]:
    """Iterate over divi archive csvs."""
    for date in iter_divi_archive_dates(start=start, end=end):
        print(f"searching for csv for date {date}")
        csv_str = pull_divi_archive_csv_by_date(date)
        yield from iter_parse_divi_archive_csv(csv_str, date=date)
        print(f"parsed csv for date {date}")


@cachetools.cached(cachetools.TTLCache(1, ttl=60 * 60))
def request_divi_api():
    response = requests.get(
        "https://www.intensivregister.de/api/public/intensivregister", timeout=30
    )
    response.raise_for_status()
    result = response.json()
    return result


def fetch_hospital_status_from_api() -> Dict[str, schemas.BedStatusUpdate]:
    """Get the hospital status from api."""
    result = request_divi_api()
    status_dict = {
        entry["id"]: schemas.BedStatusUpdate(
            low_care=bed_status["statusLowCare"],
            high_care=bed_status["statusHighCare"],
            ecmo=bed_status["statusECMO"],
            published=datetime.datetime.fromisoformat(
                entry["meldezeitpunkt"].rstrip("Z")
            ),
        )
        for entry in result["data"]
        if (bed_status := entry.get("bettenStatus"))
    }
    return status_dict


def fetch_hospital_locations() -> Dict[str, schemas.Location]:
    """Get all hospital locations from api."""
    result = request_divi_api()
    location_dict = {
        entry["id"]: schemas.Location.parse_obj(location)
        for entry in result["data"]
        if (location := entry["krankenhausStandort"]["position"])
    }
    return location_dict


def fill_archive_db(start: datetime.date = None, end: datetime.date = None) -> None:
    """Very simple method of filling database.

    Download everything, remove all records from the database and reinsert the new data.
    """
    logger.info("Fetching data from api.")
    records = [r.dict() for r in iter_divi_archive_csv(start=start, end=end)]

    logger.info("Storing data in db.")
    with database.transaction():
        database.execute(divi_archive_table.delete())
        database.execute(divi_archive_table.insert(), records)
    logger.info("Stored data in db.")


def from_archive_db(state_id: str = None) -> List[schemas.ArchiveRecord]:
    table = divi_archive_table
    query = table.select().where(table.c.district_id.isnot(None))
    if state_id is not None:
        query = query.where(table.c.state_id == state_id)
    records = database.execute(query).fetchall()
    return parse_obj_as(List[schemas.ArchiveRecord], records)
