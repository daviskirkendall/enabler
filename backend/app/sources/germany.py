"""German regions.

Raw data can be obtained from:

https://www.destatis.de/DE/Themen/Laender-Regionen/Regionales/Gemeindeverzeichnis/_inhalt.html

under "Regionale Gliederung" -> "Quartalsausgabe" (download ASC file format and extract zip file)
"""
import logging
from pathlib import Path
from typing import DefaultDict, Iterable, Iterator, List, Sequence, Tuple

import openpyxl
import requests

from app.db import database
from app.models.germany import german_communities_table, german_districts_table
from app.schemas import germany as schemas

CACHE_FOLDER = Path(".cache/germany")

logger = logging.getLogger(__name__)


STATE_ID_MAP = {
    "01": "Schleswig-Holstein",
    "02": "Freie und Hansestadt Hamburg",
    "03": "Niedersachsen",
    "04": "Freie Hansestadt Bremen",
    "05": "Nordrhein-Westfalen",
    "06": "Hessen",
    "07": "Rheinland-Pfalz",
    "08": "Baden-Württemberg",
    "09": "Freistaat Bayern",
    "10": "Saarland",
    "11": "Berlin",
    "12": "Brandenburg",
    "13": "Mecklenburg-Vorpommern",
    "14": "Freistaat Sachsen",
    "15": "Sachsen-Anhalt",
    "16": "Freistaat Thüringen",
}


def iter_rows_from_excel_file(filename, filter_row_type: str = None) -> Iterator[Tuple]:
    wb = openpyxl.load_workbook(filename, read_only=True, keep_links=False)
    sheet = wb[wb.sheetnames[1]]
    for row in sheet.iter_rows(min_row=7, max_col=20, values_only=True):
        row_type = row[0]  # satz art
        if row_type is None:
            break  # we have reached the end of the data
        if filter_row_type and str(row_type) != filter_row_type:
            continue
        yield row


def iter_communities_from_excel_file(filename) -> Iterator[schemas.Community]:
    """Iterate over communities found in the give excel file."""
    community_row_type = "60"  # we are only interested in communities
    for row in iter_rows_from_excel_file(filename, filter_row_type=community_row_type):
        # Note that for some reason row 10 (date) in the excel is not present in the row tuple
        # All later rows are thus shifted by one index.
        community_id = "".join(str(k) for k in row[2:7])
        longitude_str, latitude_str = row[15:17]
        if not latitude_str or not longitude_str:
            # Don't care about communities that don't have a location
            logging.debug(f"Community without location found : {community_id}")
            continue
        community = schemas.Community(
            community_id=community_id,
            name=row[7],
            area=row[8],
            population=row[11],
            zip_code=row[14],
            latitude=float(latitude_str.replace(",", ".")),
            longitude=float(longitude_str.replace(",", ".")),
        )
        if not (40 < community.latitude < 60) or not (4 < community.longitude < 18):
            logger.info(f"Inplausible location data. Skipping community: {community}")
            continue
        logger.debug(f"Found community {community.community_id}")
        yield community


def iter_districts_from_excel_file(filename) -> Iterator[schemas.District]:
    """Iterate over districts found in the given excel file."""
    district_row_type = "40"  # we are only interested in districts
    for row in iter_rows_from_excel_file(filename, filter_row_type=district_row_type):
        district = schemas.District(
            district_id="".join(str(k) for k in row[2:5]), name=row[7],
        )
        logger.debug(f"Found district {district.district_id}")
        yield district


def aggregate_district(
    district: schemas.District, district_communities: List[schemas.Community]
) -> schemas.AggregatedDistrict:
    if not district_communities:
        raise ValueError("Communities list must not be empty.")
    latitude, longitude = polulation_center(district_communities)

    if not (40 < latitude < 60) or not (4 < longitude < 18):
        raise ValueError(f"Inplausible location data. Skipping community: {district}")

    aggregated_district = schemas.AggregatedDistrict(
        district_id=district.district_id,
        name=district.name,
        latitude=latitude,
        longitude=longitude,
        population=sum(c.population for c in district_communities),
        area=sum(c.area for c in district_communities),
    )
    return aggregated_district


def iter_aggregate_districts(
    districts: Iterable[schemas.District], communities: Iterable[schemas.Community]
) -> Iterator[schemas.AggregatedDistrict]:
    district_community_map: DefaultDict[str, List[schemas.Community]] = DefaultDict(
        list
    )
    for community in communities:
        district_community_map[community.district_id].append(community)
    district_map = {d.district_id: d for d in districts}
    for district_id, district_communities in district_community_map.items():
        try:
            district = district_map[district_id]
        except KeyError:
            district = schemas.District(
                district_id=district_id, name=f"Unknown ({district_id})"
            )
        yield aggregate_district(district, district_communities)


def polulation_center(communities: Sequence[schemas.Community]) -> Tuple[float, float]:
    """Calculate the geographic center weighted by community population.

    WARNING: This is geographically not correct since we don't account for
    curvature.  We assume the error to be small enough to ignore (small distances).
    Also we assume that we are not at the edges of the lat/lon scope (0, 360 degrees)
    """
    n = len(communities)
    if n == 0:
        raise ValueError("Must pass at least one community.")
    total_population = sum(c.population for c in communities)
    if total_population == 0:
        latitude = sum(c.latitude for c in communities) / n
        longitude = sum(c.longitude for c in communities) / n
    else:
        latitude = (
            sum(c.latitude * c.population for c in communities) / total_population
        )
        longitude = (
            sum(c.longitude * c.population for c in communities) / total_population
        )
    return latitude, longitude


def download_excel_file() -> Path:
    """Download the excel file holding the data."""
    CACHE_FOLDER.mkdir(parents=True, exist_ok=True)
    url = "https://www.destatis.de/DE/Themen/Laender-Regionen/Regionales/Gemeindeverzeichnis/Administrativ/Archiv/GVAuszugQ/AuszugGV2QAktuell.xlsx?__blob=publicationFile"
    filepath = CACHE_FOLDER / "gemeinden.xlsx"
    if filepath.exists():
        return filepath
    response = requests.get(url, timeout=10)
    response.raise_for_status()
    content_type = response.headers.get("Content-Type")
    if (
        content_type
        != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8"
    ):
        raise ValueError(
            f"Expected a content type to signal an excel file. Got {content_type} instead"
        )
    with open(filepath, "wb") as f:
        f.write(response.content)
    return filepath


def fill_db():
    logger.info("Searching for communities...")
    filepath = download_excel_file()
    communities = list(iter_communities_from_excel_file(filepath))
    logger.info("Searching for districts...")
    tmp_districts = list(iter_districts_from_excel_file(filepath))
    logger.info("Aggregating communities into districts...")
    districts = list(
        iter_aggregate_districts(communities=communities, districts=tmp_districts)
    )

    logger.info("Inserting data into database.")
    with database.transaction():
        database.execute(german_communities_table.delete())
        database.execute(
            german_communities_table.insert([c.dict() for c in communities])
        )
        database.execute(german_districts_table.delete())
        database.execute(german_districts_table.insert([d.dict() for d in districts]))
    logger.info("Database successfully updated.")


def districts_from_db(state_id: str = None) -> List[schemas.AggregatedDistrict]:
    """Fetch all districts from database."""
    query = german_districts_table.select()
    if state_id is not None:
        query = query.where(german_districts_table.c.state_id == state_id)
    records = database.execute(query).fetchall()
    return [schemas.AggregatedDistrict.parse_obj(r) for r in records]


def communities_from_db() -> List[schemas.Community]:
    """Fetch communities from database."""
    records = database.execute(german_communities_table.select()).fetchall()
    return [schemas.Community.parse_obj(r) for r in records]
