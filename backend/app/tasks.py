"""Executing tasks."""
import copy
import datetime
import logging
import pydoc
import random
from functools import lru_cache, partial
from pathlib import Path
from time import sleep
from typing import Any, Dict, List, Optional, TypeVar, Union

import pq.tasks
import psycopg2
import sqlalchemy as sa
from pq import Job
from pq.utils import convert_time_spec, prepared, utc_format
from pydantic import BaseModel, Field, ValidationError

from app import db

logger = logging.getLogger(__name__)


class RetrySpec(BaseModel):
    """Specification for handling retries."""

    initial_delay: int = Field(0, description="Delay to trigger before first retry")
    delay_multiplier: int = Field(
        1,
        description="Delay multiplier for exponential backoff (factor with which the delays muliply on each retry).",
    )
    max_delay: Optional[int] = Field(
        None,
        description="For a single retry, do not wait longer than this number of seconds (even if the exponential backoff would have us wait longer).",
    )
    max_retries: Optional[int] = Field(
        0, description="Maximum number of retries to trigger."
    )

    def calculate_delay(self, n: int = 0) -> int:
        """Calculate the delay after `n` retries."""
        delay = self.initial_delay + self.delay_multiplier * ((2 ** n) - 1)
        if self.max_delay is not None:
            delay = min(self.max_delay, delay)
        return delay


class StrictRetrySpec(RetrySpec):
    class Config:
        extra = "forbid"


class TaskData(BaseModel):

    function: str
    args: List = []
    kwargs: Dict[str, Any] = {}
    retry: RetrySpec = RetrySpec()
    retried: int = 0


TimeSpec = Union[str, datetime.datetime, datetime.timedelta]
T = TypeVar("T", bound="Task")


class Task:
    """Object for enqueing tasks in queue.

    Args:
        f: Function to run when task executes
        queue: Task queue that the task is bound to.
        schedule_at, expected_at: Passed to pq job
        retry: Retry rules for task.
        pass_job: If set to `True` passes the job as the first argument to `f` when executing.

    """

    def __init__(
        self,
        f,
        queue,
        schedule_at: TimeSpec = None,
        expected_at: TimeSpec = None,
        retry: Union[dict, RetrySpec] = None,
        pass_job: bool = False,
    ):
        self.queue = queue
        self.f = f
        self.function_path = f"{f.__module__}.{f.__qualname__}"
        self.queue.handler_registry[self.function_path] = self
        self.schedule_at = schedule_at
        self.expected_at = expected_at
        self.retry = StrictRetrySpec.parse_obj(retry) if retry else StrictRetrySpec()
        self.pass_job = pass_job
        # Decorated functions should behave the same way as before when they are called.

    def __call__(self, *args, **kwargs):
        return self.f(*args, **kwargs)

    def using(
        self: T,
        schedule_at: Optional[TimeSpec] = "IGNORE",
        expected_at: Optional[TimeSpec] = "IGNORE",
    ) -> T:
        """Create a copy of this task with overridden options."""
        new = copy.copy(self)
        if schedule_at != "IGNORE":
            new.schedule_at = schedule_at
        if expected_at != "IGNORE":
            new.expected_at = expected_at
        return new

    def enqueue(self, *args, **kwargs):
        """Put the task on the queue."""
        with self.queue._transaction() as cursor:
            return self.queue._put_item(
                cursor, *list(self.sql_values(*args, **kwargs).values())[1:]
            )

    def sql_values(self, *args, **kwargs) -> Dict[str, Any]:
        """Convert task settings to sql compatible values."""
        task_data = TaskData(
            function=self.function_path, args=args, kwargs=kwargs, retry=self.retry,
        )
        return {
            "q_name": self.queue.name,
            "data": task_data.json(),
            "schedule_at": self._time_spec_to_str(self.schedule_at),
            "expected_at": self._time_spec_to_str(self.expected_at),
        }

    def _time_spec_to_str(self, t):
        t = convert_time_spec(t)
        return None if t is None else utc_format(t)

    @classmethod
    def decorator(
        cls,
        queue,
        schedule_at: str = None,
        expected_at: str = None,
        retry: RetrySpec = None,
        pass_job=False,
    ):
        return partial(
            cls,
            queue=queue,
            schedule_at=schedule_at,
            expected_at=expected_at,
            retry=retry,
            pass_job=pass_job,
        )


class TaskQueue(pq.tasks.Queue):
    """Specialized task queue for handling retries."""

    task = Task.decorator

    def perform(self, job: Job) -> bool:
        """Process job and run the function associated with the jobs task."""
        try:
            data = TaskData.parse_obj(job.data)
        except ValidationError as e:
            no_retry = TaskData(function="INVALID", retry=RetrySpec(max_retries=0))
            return self.fail(job, no_retry, e)
        try:
            f = self.handler_registry[data.function]
        except KeyError:
            f = self.handler_registry[data.function] = pydoc.locate(data.function)
        if f is None:
            return self.fail(
                job, data, KeyError("Job handler `{data.function}` not found.",)
            )

        # If the task wants the job to be passed to the underlying function we
        # want to add the job as an argument.
        job_args = (job,) if getattr(f, "pass_job", False) else ()
        try:
            f(*job_args, *data.args, **data.kwargs)
            return True

        except Exception as e:
            return self.fail(job, data, e)

    def fail(self, job: Job, data: TaskData, e: Exception = None) -> bool:
        """Handle job failure gracefully."""
        retried = data.retried

        if data.retry.max_retries is None or data.retry.max_retries > data.retried:
            delay = data.retry.calculate_delay(retried)
            data.retried += 1
            id = self.put(data.dict(), schedule_at=f"{int(delay)}s")
            self.logger.warning(
                "Failed to perform job %r. Rescheduled as `%s`" % (job, id),
                exc_info=True,
            )
            return False

        self.logger.exception("Failed to perform job %r :" % job)
        return False

    def work(self, burst: bool = False) -> None:
        """Fetch jobs from the database and process them.

        If "burst" is `True` process only until there are no more jobs in the queue.
        Otherwise wait for new jobs to be uploaded until the database
        connection is severed.
        """
        queue.logger.info("Waiting for new jobs...")
        while True:
            with self as cursor:
                job = self.get(self._get_timeout())
                if job is None:
                    self.logger.debug("No new jobs found, waiting...")
                    if burst:
                        return
                    continue
                self.logger.info("Found job %s. Starting work...", job.id)
                success = self.perform(job)
                if success:
                    self.logger.info("Job %s succeeded.", job.id)
                # We remove the item directly (as opposed to keeping it in the table forever.)
                self._delete_item(cursor, job.id)
                queue.logger.info("Job %s removed from queue.", job.id)

    def _get_timeout(self) -> float:
        """Calculate the timeout used when polling the database for tasks."""
        return 60 + 60 * random.random()

    @prepared
    def _delete_item(self, cursor):
        """Removes a single item from the queue.

            DELETE FROM %(table)s WHERE id = $1
            RETURNING length(data::text)

        Note: The "prepared" decorator executes the sql above with the
        correct arguments.
        """
        return cursor.fetchone()[0]


# SQLALCHEMY INTEGRATION


@lru_cache
def get_queue_table(name):
    """Get an sqlalchemy table definition for a queue with the given name.

    The tables are cached so that the name will always return the same table
    object.

    Note that we don't specify the exact types (dates and json) because we
    need to stay compatible with the raw db drivers (psycopg for example).
    """
    return sa.table(
        name,
        sa.column("id", sa.Integer),
        sa.column("schedule_at"),
        sa.column("expected_at"),
        sa.column("q_name", sa.String),
        sa.column("data"),
    )


S = TypeVar("S", bound="SqlaTaskHandler")


class SqlaTaskHandler:
    """Helper object for creating sqlalchemy expressions for tasks."""

    def __init__(self, task: Task) -> None:
        self.task = task

    @property
    def table(self) -> sa.Table:
        return SqlaQueueHandler(self.task.queue).table

    def insert(self, *args, **kwargs) -> sa.sql.Insert:
        """Build sql expression for inserting a task job into the queue.

        If inserted and processed the task will be called with *args, **kwargs as arguments.
        """
        return (
            self.table.insert()
            .values(self.task.sql_values(*args, **kwargs))
            .returning(self.table.c.id)
        )

    def delete(self, id: int) -> sa.sql.Delete:
        """Build sql expression for deleting the job with a given id."""
        return self.table.delete().where(self.table.c.id == id)


class SqlaQueueHandler:
    """Helper object for creating sqlalchemy expressions for queues."""

    def __init__(self, queue: TaskQueue):
        self.queue = queue

    @property
    def table(self) -> sa.Table:
        return get_queue_table(str(self.queue.table))

    def create(self) -> sa.Text:
        """Sql expression for creating task queue table."""
        sql = (Path(pq.tasks.PQ.template_path) / "create.sql").read_text()
        tablename = str(self.queue.table)
        if not tablename.isidentifier():
            raise ValueError(
                "Queue tablename should be a valid identifier. Got: {repr(tablename)}."
            )
        sql = sql.replace("%(name)s", tablename)
        return sa.text(sql)


class SqlaTask(Task):
    """Task with sqlalchemy integration."""

    @property
    def sql(self) -> SqlaTaskHandler:
        return SqlaTaskHandler(self)


class SqlaTaskQueue(TaskQueue):
    """Task queue with sqlalchemy integration."""

    @property
    def sql(self) -> SqlaQueueHandler:
        """Sqlalchemy expression creation helpers."""
        return SqlaQueueHandler(self)

    @property
    def task(self):
        return partial(SqlaTask.decorator, self)


queue = SqlaTaskQueue(name="tasks", table="queue")


def create_queue_table(queue: SqlaTaskQueue):
    """Create the database table and relevant objects for the passed queue.

    Does nothing if the table already exists.
    """
    logger.info(f"Creating queue table: {str(queue.table)}")

    try:
        db.database.execute(queue.sql.table.select().limit(0))
    except sa.exc.ProgrammingError as e:
        if "does not exist" not in str(e):
            raise
    else:
        logger.info(f"Queue table: {str(queue.table)} already exists.")
        return

    try:
        db.database.execute(queue.sql.create())
    except sa.exc.OperationalError as e:
        if "deadlock detected" not in str(e):
            # assuming we someone else is using the table which is fine as
            # obviously it has been created.
            raise
        logger.info(f"Queue table: {str(queue.table)} already exists.", exc_info=True)
    else:
        logger.info(f"Created queue table: {str(queue.table)}")


def work_forever(queue: SqlaTaskQueue):
    """Work on task queue forever.

    Establishes and manages database connections, pulls items from the queue
    and processes them forever.
    """
    create_queue_table(queue)
    while True:
        con = db.database.connect()
        try:
            queue.conn = con.connection.connection
            queue.work()
        except (sa.exc.OperationalError, psycopg2.InterfaceError):
            # Since the queue uses a raw dbapi connection, we need to make sure
            # that the sqlalchemy connection also knows that something went
            # wrong so it can recreate itself.
            con.invalidate()
            delay = int(2 + 2 * random.random())
            logger.warning(
                f"Something went wrong communicating with the database.  Retrying in {delay}s",
                exc_info=True,
            )
            sleep(delay)
