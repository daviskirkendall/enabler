import logging
import pathlib
import shlex
import subprocess
import sys
from typing import List, Sequence, Union

import click

logger = logging.getLogger(__name__)


@click.group()
def dev():
    """Development helpers.

    Includes formatters, linters, checks and tests.
    """


@dev.command()
def format():
    """Format code using isort and black."""
    base_path = get_default_path("")
    click.secho("sorting imports using isort", fg="cyan", bold=True)
    isort_config = get_default_path(".isort.cfg")
    run_cmd(f"isort --settings-path={isort_config} {base_path}", exit_on_error=True)

    click.secho("painting all code black", fg="cyan", bold=True)
    run_cmd(f"black {base_path}", exit_on_error=True)

    click.secho("All code formatted!", fg="green", bold=True)


@dev.command()
def check():
    """Check style and types using flake8 and mypy."""
    base_path = get_default_path("")
    click.secho(
        "check that code is formatted correctly (isort+black)", fg="cyan", bold=True
    )
    isort_config = get_default_path(".isort.cfg")
    isort_result = run_cmd(
        f"isort --check-only --diff --settings-path={isort_config} {base_path}"
    )
    black_result = run_cmd(f"black --check --diff {base_path}")

    click.secho("running flake8 (stylechecking)", fg="cyan", bold=True)
    flake8_config = get_default_path(".flake8")
    flake8_result = run_cmd(f"flake8 --config={flake8_config} {base_path}")

    click.secho("running mypy (typechecking)", fg="cyan", bold=True)
    mypy_config = get_default_path("mypy.ini")
    mypy_result = run_cmd(f"mypy --config-file={mypy_config} {base_path}")

    # exit with non-zero code if one of the checks failed
    exit_on_error([isort_result, black_result, flake8_result, mypy_result])
    click.secho("All checks passed!", fg="green", bold=True)


@dev.command(context_settings={"ignore_unknown_options": True})
@click.argument("args", nargs=-1, type=click.UNPROCESSED)
def test(args):
    """Run tests and report coverage.

    Pytest is used to run tests and any additional arguments are passed to
    pytest, allowing you to filter tests if necessary or run other options.

    For example:

    python run.py dev test -k mytest -x --pdb

    will run tests that have "mytest" in their name, stop on the first test
    that errors out and drop you into the debugger at the point of the
    failure.
    """
    click.secho("running pytest", fg="cyan", bold=True)
    pytest_result = run_cmd(["coverage", "run", "-m", "pytest"] + list(args))
    run_cmd(["coverage", "html"])  # don't care about the result of this
    coverage_result = run_cmd(["coverage", "report"])

    # If either pytest (any failing test) or coverage fail (minimum coverage not
    # reached), we interpret the job as failed.
    exit_on_error([pytest_result, coverage_result])
    click.secho("All checks passed!", fg="green", bold=True)


@dev.command()
@click.pass_context
def all(ctx):
    """Run formatting, checks and tests.

    This is helpful if you want to push code. All of these checks are also run
    in CI, so if these all pass you'll have a good chance that the CI jobs will
    be green as well.
    """
    ctx.invoke(format)
    ctx.invoke(check)
    ctx.invoke(test)


@dev.command()
@click.pass_context
def run(ctx):
    """Run api and worker.

    Note that this is an easy way to run the api and workers using one
    command, but if you need more control over the these commands, you should
    use their dedicated commands ("api run" and "worker run").
    """
    import subprocess
    import sys
    from concurrent.futures import ThreadPoolExecutor

    with ThreadPoolExecutor() as executor:
        executor.submit(
            subprocess.run,
            [sys.executable, *sys.argv[:-2], "worker", "run", "--reload"],
        )
        executor.submit(
            subprocess.run, [sys.executable, *sys.argv[:-2], "api", "run", "--reload"]
        )


def run_cmd(
    cmd: Union[str, Sequence[str]], exit_on_error: bool = False, **kwargs
) -> subprocess.CompletedProcess:
    """Run command and exit on error.

    Args:
        cmd: command to run
        exit_on_error: If set, exit the entire program if the return code indicates an error.
        reload: If set, restart if any python files in the project change.
    """
    command_list: List[str] = shlex.split(cmd) if isinstance(cmd, str) else list(cmd)
    result = subprocess.run(command_list, **kwargs)
    if exit_on_error and result.returncode != 0:
        sys.exit(result.returncode)
    return result


def exit_on_error(results: List[subprocess.CompletedProcess]) -> None:
    """Exit entire program with error if any of the passes completed process results exits with an error."""
    failed = [result for result in results if result.returncode != 0]

    if failed:
        failed_str_list = ["'" + " ".join(result.args) + "'" for result in failed]
        failed_str = ", ".join(failed_str_list)
        click.secho(
            f"{len(failed)} commands ({failed_str}) failed. Exiting",
            fg="red",
            bold=True,
        )
        sys.exit(failed[0].returncode)


def get_default_path(name: str):
    """Return the path to default files."""
    path = pathlib.Path(__file__).parent / ".." / ".." / name
    return str(path).replace("\\", "/")
