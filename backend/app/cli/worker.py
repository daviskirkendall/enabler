import logging
import logging.config

import click

from app.cli.reload import reload_option

logger = logging.getLogger(__name__)


@click.group()
def worker():
    """Commands for running worker."""


@worker.command()
@reload_option()
def run(**kwargs):
    """Run the API.

    All additional options and arguments are passed to the "uvicorn" command.
    The default options are loaded by climatecontrol.
    """
    from app import tasks

    tasks.work_forever(tasks.queue)
