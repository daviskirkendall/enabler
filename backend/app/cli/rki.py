import click


@click.group()
def rki():
    """Utilities for working wih RKI (Robert Koch Institut) data."""


@rki.command()
@click.option("--limit", default=None, type=int)
def fill_db(limit=None):
    """Repopulate database with newest data obtained from rki."""
    from app.db import init_db
    from app.sources.rki import fetcher

    init_db()
    fetcher.fill_db(limit=limit)
