import logging

import click

from app.cli import api, worker
from app.cli.dev import dev
from app.cli.divi import divi
from app.cli.germany import germany
from app.cli.optimization import optimization
from app.cli.rki import rki

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


@click.group()
def cli():
    """Command line interface."""
    logging.basicConfig(level=logging.INFO)


cli.add_command(api.api)
cli.add_command(worker.worker)
cli.add_command(rki)
cli.add_command(divi)
cli.add_command(germany)
cli.add_command(optimization)
cli.add_command(dev)


if __name__ == "__main__":
    cli()
