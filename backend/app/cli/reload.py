import logging
import pathlib

import click

logger = logging.getLogger(__name__)


def reload_option():
    def callback(ctx, param, value):
        return watch_reload()

    return click.option(
        "--reload", is_flag=True, help="reload code when it changes", callback=callback
    )


def watch_reload() -> None:
    """Run hupper reloader."""
    try:
        import hupper
    except ModuleNotFoundError:
        logger.warning(
            'Not running file watcher/reloader as "hupper" is not installed.  Try installing it with "pip install hupper"'
        )
        return

    reloader = hupper.start_reloader("app.cli.cli", shutdown_interval=10, verbose=2)
    reloader.watch_files(pathlib.Path(__file__).parent.parent.glob("**/*.py"))
