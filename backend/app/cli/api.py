import logging
import logging.config

import click
import uvicorn

from app.cli.reload import reload_option

logger = logging.getLogger(__name__)


@click.group()
def api():
    """Commands for running API."""


def _uvicorn_api_options(f):
    """Add uvicorn cli options to click command.

    The uvicorn cli is also based on click so we'll just add the options from
    there directly.

    """

    for option in uvicorn.main.params:
        if not isinstance(option, click.Option):
            continue
        f.params.append(option)


@_uvicorn_api_options
@api.command()
@reload_option()
def run(**kwargs):
    """Run the API.

    All additional options and arguments are passed to the "uvicorn" command.
    The default options are loaded by climatecontrol.
    """
    from app import db
    from app.api import app

    # create all database tables before we start.
    db.init_db()

    # Run app
    uvicorn.run(app, **kwargs)
