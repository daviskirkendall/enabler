import click


@click.group()
def divi():
    """Utilities for working with DIVI data.

    DIVI (Deutschen Interdisziplinären Vereinigung für Intensiv- und Notfallmedizin)
    """
    pass


@divi.command()
def poll_archive(limit=None):
    """Repopulate the database with newest archive DIVI data."""
    from app.db import init_db
    from app.sources.divi import archive

    init_db()
    archive.fill_archive_db()
