import click


@click.group()
def germany():
    """Utilities for working wih german local government unit data."""


@germany.command()
@click.option("--limit", default=None, type=int)
def fill_db(limit=None):
    """Repopulate database with newest data."""
    import app.sources.germany
    from app.db import init_db

    init_db()
    app.sources.germany.fill_db()
