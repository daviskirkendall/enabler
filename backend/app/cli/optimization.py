import click


@click.group()
def optimization():
    "Utilities for running the optimization model."
    pass


@optimization.command()
@click.option(
    "--filename",
    default="examples/config.json",
    type=click.Path(dir_okay=False),
    help="Relative path incl. filename under which the config file shall be saved as json",
)
@click.option(
    "--state-id",
    default=None,
    type=str,
    help="Two-digit-code representing the German states. Used to limit config file to those states.",
)
@click.option(
    "--start-date",
    default="2020-04-04",
    type=click.DateTime(),
    help="Starting date of optimization period (YYYY-MM-DD). Earliest date possible: 2020-04-04.",
)
@click.option(
    "--end-date",
    default="2020-05-04",
    type=click.DateTime(),
    help="End date of optimization period (YYYY-MM-DD)",
)
@click.option(
    "--discharge-days",
    default=7,
    type=int,
    help="Number of days it would take to release the patients currently occuping the intensive beds",
)
@click.option(
    "--usual-avg-healing_time",
    default=7,
    type=int,
    help="Average number of days a normal patient spends in intensive care",
)
@click.option(
    "--covid-avg-healing_time",
    default=10,
    type=int,
    help="Average number of days a covid patient spends in intensive care",
)
@click.option(
    "--time-covid-to-intensive",
    default=5,
    type=int,
    help="Number of days it takes for a covid patient to show up in intensive care",
)
@click.option(
    "--intensive-covid-fraction",
    default=0.1,
    type=float,
    help="Percentage of covid cases that turns up in intensive care",
)
@click.option(
    "--covid-case-factor",
    default=1,
    type=float,
    help=(
        "Factor with which to scale total number of cases. "
        "A factor of 2 will result in double the patients."
    ),
)
def config(
    filename,
    state_id,
    start_date,
    end_date,
    discharge_days,
    usual_avg_healing_time,
    covid_avg_healing_time,
    time_covid_to_intensive,
    intensive_covid_fraction,
    covid_case_factor,
):
    from examples.create_example import create_config_file

    create_config_file(
        filename=filename,
        state_id=state_id,
        start_date=start_date.date(),
        end_date=end_date.date(),
        discharge_days=discharge_days,
        usual_avg_healing_time=usual_avg_healing_time,
        covid_avg_healing_time=covid_avg_healing_time,
        time_covid_to_intensive=time_covid_to_intensive,
        intensive_covid_fraction=intensive_covid_fraction,
        covid_case_factor=covid_case_factor,
    )


@optimization.command()
@click.option(
    "--config-filename",
    default="examples/config.json",
    help='Relative path of configuration file, e.g. "examples/scenarion.json"',
)
@click.option(
    "--max-distance",
    default=None,
    type=int,
    help="Maximum distance between two districts that is allowed for reallocation.",
)
def run(config_filename, max_distance):
    from app.optimization.matching import run_matching

    run_matching(config_filename, max_distance)


@optimization.command()
@click.option(
    "--results-filename",
    default="examples/results_config.json",
    help="Relative path of optimization results file",
)
@click.option(
    "--gif-filename", type=click.Path(dir_okay=False), default="examples/plot.gif"
)
def create_gif(results_filename, gif_filename):
    import tempfile

    from examples.create_example import Plotter

    plotter = Plotter(results_filename)

    plotter.save_timeseries_plot(f"{gif_filename.split('.')[-2]}.png")

    with tempfile.TemporaryDirectory() as tmpdirname:
        plotter.save_map_plots(tmpdirname)
        plotter.create_gif(gif_filename, source_dirname=tmpdirname)
