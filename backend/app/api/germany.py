from typing import List

from fastapi import APIRouter

from app.schemas import germany as schemas
from app.sources.germany import districts_from_db

router = APIRouter()


@router.get(
    "/districts", response_model=List[schemas.AggregatedDistrict],
)
def get_districts():
    return districts_from_db()
