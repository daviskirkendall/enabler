from typing import List

from fastapi import APIRouter

from app.schemas import rki as schemas
from app.sources.rki.fetcher import daily_cases_from_db

router = APIRouter()


@router.get(
    "/cases", response_model=List[schemas.DailyCaseSummary],
)
def get_daily_cases(district_id: str = None, state_id: str = None):
    return daily_cases_from_db(district_id=district_id, state_id=state_id)
