from typing import List

from fastapi import APIRouter

from app.schemas import divi as schemas
from app.sources.divi.archive import from_archive_db

router = APIRouter()


@router.get("/archive/records", response_model=List[schemas.ArchiveRecord])
def get_archive():
    return from_archive_db()
