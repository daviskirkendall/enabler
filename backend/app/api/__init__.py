"""API package."""

from fastapi import FastAPI

from app.api import divi, germany, optimization, rki

app = FastAPI(title="3_21_patientenverteilung")

app.include_router(rki.router, prefix="/api/rki", tags=["RKI"])
app.include_router(divi.router, prefix="/api/divi", tags=["DIVI"])
app.include_router(germany.router, prefix="/api/germany", tags=["Germany"])
app.include_router(
    optimization.router, prefix="/api/optimizations", tags=["Optimization"]
)
