from typing import List

import sqlalchemy as sa
from fastapi import APIRouter, HTTPException

from app.db import database
from app.models.optimization import optimizations_table
from app.optimization.tasks import create_new_optimization, run_optimization
from app.schemas.optimization import (
    CreatedJob,
    JobInputSchema,
    JobSchema,
    ModelConfiguration,
)

router = APIRouter()


@router.post("", response_model=CreatedJob)
def trigger_optimization(item: JobInputSchema):

    with database.transaction():
        optimization_id = create_new_optimization(item.configuration)
        job_queue_id = database.execute(
            run_optimization.sql.insert(id=optimization_id,)
        ).scalar()
        assert job_queue_id is not None

        database.execute(
            optimizations_table.update()
            .where(optimizations_table.c.id == optimization_id)
            .values(job_queue_id=job_queue_id)
        )

        created = database.execute(
            optimizations_table.select().where(
                optimizations_table.c.id == optimization_id
            )
        ).first()

    return dict(created)


@router.get("/{id}", response_model=JobSchema)
def get_optimization(id: int):
    """Get an optimization job identified by the given id."""
    query = _get_optimizations_query().where(optimizations_table.c.id == id)
    with database.transaction():
        result = database.execute(query).first()
    if result is None:
        raise HTTPException(404, detail="This optimization could not be found.")
    return dict(result)


@router.get("", response_model=List[JobSchema])
def get_optimizations():
    """List all optimizations."""
    query = (
        _get_optimizations_query()
        .order_by(sa.desc(optimizations_table.c.created_at))
        .limit(1000)
    )
    with database.transaction():
        result = database.execute(query).fetchall()
    return [dict(item) for item in result]


@router.get("/{id}/configuration", response_model=ModelConfiguration)
def get_optimization_configuration(id: int):
    query = sa.select([optimizations_table.c.configuration]).where(
        optimizations_table.c.id == id
    )
    with database.transaction():
        result = database.execute(query).first()
    if result is None:
        raise HTTPException(404, detail="This optimization could not be found.")
    return result.configuration


@router.get("/{id}/result", response_model=dict)
def get_optimization_result(id: int):
    query = sa.select([optimizations_table.c.result]).where(
        optimizations_table.c.id == id
    )
    with database.transaction():
        result = database.execute(query).first()
    if result is None:
        raise HTTPException(404, detail="This optimization result could not be found.")
    return result.result


def _get_optimizations_query() -> sa.sql.Select:
    t = optimizations_table
    columns = [c for c in t.c if c not in {t.c.configuration, t.c.error, t.c.result}]
    query = sa.select(columns)
    return query
