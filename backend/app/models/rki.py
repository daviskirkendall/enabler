import sqlalchemy as sa
from sqlalchemy import Column, Date, DateTime, Integer, String

from app.db import metadata

rki_covid19_cases_table = sa.Table(
    "rki_covid_19_cases",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("object_id", Integer),
    Column("reported_at", DateTime, index=True),
    Column("referenced_at", DateTime, index=True),
    Column("updated_at", DateTime),
    Column("case_type", sa.String(), index=True),
    Column("district_id", String(5), index=True),
    Column("state_id", String(2), index=True),
    Column("gender", sa.String()),
    Column("age_group", String),
    Column("reference_date", Date, index=True),
)
