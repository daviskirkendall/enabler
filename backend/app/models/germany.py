from sqlalchemy import Column, Float, Integer, String, Table

from app.db import metadata

german_communities_table = Table(
    "german_communities",
    metadata,
    Column("community_id", String(12), primary_key=True),
    Column("name", String, nullable=False),
    Column("district_id", String(5), index=True, nullable=False),
    Column("state_id", String(2), index=True, nullable=False),
    Column("population", Integer, nullable=False),
    Column("area", Float, nullable=False),
    Column("latitude", Float, nullable=False),
    Column("longitude", Float, nullable=False),
    Column("zip_code", String),
)

german_districts_table = Table(
    "german_districts",
    metadata,
    Column("district_id", String(5), primary_key=True),
    Column("name", String, nullable=False),
    Column("state_id", String(2), index=True, nullable=False),
    Column("population", Integer, nullable=False),
    Column("area", Float, nullable=False),
    Column("latitude", Float, nullable=False),
    Column("longitude", Float, nullable=False),
)
