import sqlalchemy as sa
from sqlalchemy import Column, DateTime, Integer, Table
from sqlalchemy.dialects.postgresql import JSONB

from app.db import metadata

optimizations_table = Table(
    "optimizations",
    metadata,
    Column("id", Integer, primary_key=True),
    Column(
        "created_at", DateTime, nullable=False, server_default=sa.func.now(), index=True
    ),
    Column(
        "updated_at",
        DateTime,
        nullable=False,
        server_default=sa.func.now(),
        onupdate=sa.func.now(),
    ),
    Column("finished_at", DateTime, index=True),
    Column("started_at", DateTime),
    Column("configuration", JSONB, nullable=False),
    Column("job_queue_id", Integer),
    Column("result", JSONB),
    Column("error", JSONB),
    Column("status", sa.String, default="CREATED", nullable=False, index=True),
)
