from sqlalchemy import (
    Column,
    Date,
    DateTime,
    Integer,
    String,
    Table,
    UniqueConstraint,
)

from app.db import metadata

divi_archive_table = Table(
    "daily_intensive_care_bed_summaries",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("state_id", String(2), index=True),
    Column("district_id", String(5), index=True),
    Column("report_area_count", Integer),
    Column("current_covid_case_count", Integer),
    Column("current_covid_case_ventilated_count", Integer),
    Column("site_count", Integer),
    Column("bed_unoccupied_count", Integer),
    Column("bed_occupied_count", Integer),
    Column("updated_at", DateTime),
    Column("date", Date),
    UniqueConstraint("district_id", "date"),
)
