from app.sources.rki.fetcher import daily_cases_from_db, fill_db


def test_rki(clean_db):
    """Test rki db fetching and filling."""
    assert len(daily_cases_from_db()) == 0
    fill_db(limit=4000)
    records = daily_cases_from_db()
    n_records = len(records)
    assert n_records > 0
    unique_combos = [(r.district_id, r.date) for r in records]
    assert len(unique_combos) == len(
        set(unique_combos)
    ), "Assert date/district combos were expected to be unique"
