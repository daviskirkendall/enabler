import logging

import pytest

from app import db, tasks
from app.settings import SETTINGS

logging.basicConfig(level=logging.INFO)


@pytest.fixture(scope="session")
def init_db():
    if "postgres:postgres@" not in SETTINGS.db_uri:
        raise ValueError(
            "You might be running tests with a non-Test Database configuration!"
            "Turn around, walk away very slowly, and don't touch anything!!"
        )
    assert db.metadata.schema == "pytest"
    db.database.execute(f"CREATE SCHEMA IF NOT EXISTS {db.metadata.schema}")


@pytest.fixture
def clean_db(init_db):
    db.metadata.drop_all(db.database.engine)
    db.metadata.create_all(db.database.engine)
    return db.database


@pytest.fixture
def simple_optimization_configuration():
    """Simple configuration example."""

    configuration = {
        "forecast_horizon": 5,
        "locations": [
            {
                "name": "Cologne",
                "longitude": 6.9,
                "latitude": 50.9,
                "occupation": 10,
                "capacity": 35,
                "discharge_forecast": [3, 2, 2, 2, 1],
                "new_patients": [
                    {
                        "category": "usual",
                        "forecast": [2, 2, 2, 2, 2],
                        "avg_healing_time": 2,
                        "relocation_possible": False,
                    },
                    {
                        "category": "covid",
                        "forecast": [5, 6, 7, 8, 9],
                        "avg_healing_time": 4,
                        "relocation_possible": True,
                    },
                ],
            },
            {
                "name": "Berlin",
                "longitude": 13.4,
                "latitude": 52.5,
                "occupation": 10,
                "capacity": 50,
                "discharge_forecast": [3, 2, 2, 2, 1],
                "new_patients": [
                    {
                        "category": "usual",
                        "forecast": [2, 2, 2, 2, 2],
                        "avg_healing_time": 2,
                        "relocation_possible": False,
                    },
                    {
                        "category": "covid",
                        "forecast": [3, 4, 5, 6, 7],
                        "avg_healing_time": 4,
                        "relocation_possible": True,
                    },
                ],
            },
            {
                "name": "Wuppertal",
                "longitude": 7.15,
                "latitude": 51.2,
                "occupation": 10,
                "capacity": 22,
                "discharge_forecast": [3, 2, 2, 2, 1],
                "new_patients": [
                    {
                        "category": "usual",
                        "forecast": [2, 2, 2, 2, 2],
                        "avg_healing_time": 2,
                        "relocation_possible": False,
                    },
                    {
                        "category": "covid",
                        "forecast": [5, 6, 7, 8, 9],
                        "avg_healing_time": 4,
                        "relocation_possible": True,
                    },
                ],
            },
        ],
        "meta": {},
    }
    return configuration


# Queue fixtures


@pytest.fixture
def queue_db():
    tasks.create_queue_table(tasks.queue)
    db.database.execute(f"DELETE FROM {tasks.queue.table}")
    with db.database.connect() as con:
        tasks.queue.conn = con.connection.connection
        yield
