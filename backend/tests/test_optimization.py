"""Test optimization."""

from app.optimization.matching import MatchingModel


def test_matching(simple_optimization_configuration):
    """Test matching example."""

    model = MatchingModel(configuration=simple_optimization_configuration)
    model.solve()
    assert model.results == {
        "allocations": [
            {
                "category": "usual",
                "from_location": "Cologne",
                "result": 2,
                "time": 0,
                "to_location": "Cologne",
            },
            {
                "category": "usual",
                "from_location": "Cologne",
                "result": 2,
                "time": 1,
                "to_location": "Cologne",
            },
            {
                "category": "usual",
                "from_location": "Cologne",
                "result": 2,
                "time": 2,
                "to_location": "Cologne",
            },
            {
                "category": "usual",
                "from_location": "Cologne",
                "result": 2,
                "time": 3,
                "to_location": "Cologne",
            },
            {
                "category": "usual",
                "from_location": "Cologne",
                "result": 2,
                "time": 4,
                "to_location": "Cologne",
            },
            {
                "category": "covid",
                "from_location": "Cologne",
                "result": 5,
                "time": 0,
                "to_location": "Cologne",
            },
            {
                "category": "covid",
                "from_location": "Cologne",
                "result": 6,
                "time": 1,
                "to_location": "Cologne",
            },
            {
                "category": "covid",
                "from_location": "Cologne",
                "result": 7,
                "time": 2,
                "to_location": "Cologne",
            },
            {
                "category": "covid",
                "from_location": "Cologne",
                "result": 8,
                "time": 3,
                "to_location": "Cologne",
            },
            {
                "category": "covid",
                "from_location": "Cologne",
                "result": 9,
                "time": 4,
                "to_location": "Cologne",
            },
            {
                "category": "usual",
                "from_location": "Berlin",
                "result": 2,
                "time": 0,
                "to_location": "Berlin",
            },
            {
                "category": "usual",
                "from_location": "Berlin",
                "result": 2,
                "time": 1,
                "to_location": "Berlin",
            },
            {
                "category": "usual",
                "from_location": "Berlin",
                "result": 2,
                "time": 2,
                "to_location": "Berlin",
            },
            {
                "category": "usual",
                "from_location": "Berlin",
                "result": 2,
                "time": 3,
                "to_location": "Berlin",
            },
            {
                "category": "usual",
                "from_location": "Berlin",
                "result": 2,
                "time": 4,
                "to_location": "Berlin",
            },
            {
                "category": "covid",
                "from_location": "Berlin",
                "result": 3,
                "time": 0,
                "to_location": "Berlin",
            },
            {
                "category": "covid",
                "from_location": "Berlin",
                "result": 4,
                "time": 1,
                "to_location": "Berlin",
            },
            {
                "category": "covid",
                "from_location": "Berlin",
                "result": 5,
                "time": 2,
                "to_location": "Berlin",
            },
            {
                "category": "covid",
                "from_location": "Berlin",
                "result": 6,
                "time": 3,
                "to_location": "Berlin",
            },
            {
                "category": "covid",
                "from_location": "Berlin",
                "result": 7,
                "time": 4,
                "to_location": "Berlin",
            },
            {
                "category": "covid",
                "from_location": "Wuppertal",
                "result": 1,
                "time": 2,
                "to_location": "Cologne",
            },
            {
                "category": "covid",
                "from_location": "Wuppertal",
                "result": 2,
                "time": 2,
                "to_location": "Berlin",
            },
            {
                "category": "covid",
                "from_location": "Wuppertal",
                "result": 6,
                "time": 3,
                "to_location": "Berlin",
            },
            {
                "category": "covid",
                "from_location": "Wuppertal",
                "result": 3,
                "time": 4,
                "to_location": "Berlin",
            },
            {
                "category": "usual",
                "from_location": "Wuppertal",
                "result": 2,
                "time": 0,
                "to_location": "Wuppertal",
            },
            {
                "category": "usual",
                "from_location": "Wuppertal",
                "result": 2,
                "time": 1,
                "to_location": "Wuppertal",
            },
            {
                "category": "usual",
                "from_location": "Wuppertal",
                "result": 2,
                "time": 2,
                "to_location": "Wuppertal",
            },
            {
                "category": "usual",
                "from_location": "Wuppertal",
                "result": 2,
                "time": 3,
                "to_location": "Wuppertal",
            },
            {
                "category": "usual",
                "from_location": "Wuppertal",
                "result": 2,
                "time": 4,
                "to_location": "Wuppertal",
            },
            {
                "category": "covid",
                "from_location": "Wuppertal",
                "result": 5,
                "time": 0,
                "to_location": "Wuppertal",
            },
            {
                "category": "covid",
                "from_location": "Wuppertal",
                "result": 6,
                "time": 1,
                "to_location": "Wuppertal",
            },
            {
                "category": "covid",
                "from_location": "Wuppertal",
                "result": 4,
                "time": 2,
                "to_location": "Wuppertal",
            },
            {
                "category": "covid",
                "from_location": "Wuppertal",
                "result": 2,
                "time": 3,
                "to_location": "Wuppertal",
            },
            {
                "category": "covid",
                "from_location": "Wuppertal",
                "result": 6,
                "time": 4,
                "to_location": "Wuppertal",
            },
        ],
        "objective": 488852.0,
        "occupation": {
            "Berlin": [12, 16, 21, 31, 37],
            "Cologne": [14, 20, 26, 32, 35],
            "Wuppertal": [14, 20, 22, 22, 22],
        },
    }


def test_over_occupation(simple_optimization_configuration):
    """Test matching example with over occupation."""

    model = MatchingModel(
        configuration=simple_optimization_configuration, max_distance=0
    )
    model.solve()
    assert model.results.occupation == {
        "Cologne": [14, 20, 25, 31, 34],
        "Berlin": [12, 16, 19, 23, 26],
        "Wuppertal": [14, 20, 25, 31, 34],
    }
    assert model.results.objective == 24000000.0
