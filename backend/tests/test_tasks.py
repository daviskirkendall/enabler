import pytest
from pq import Job

from app import db
from app.tasks import (
    RetrySpec,
    SqlaQueueHandler,
    create_queue_table,
    queue,
    work_forever,
)


def test_create_queue_table(mocker, queue_db):
    mocked_create = mocker.spy(SqlaQueueHandler, "create")
    create_queue_table(queue)
    assert (
        mocked_create.call_count == 0
    ), "Expected table to already exist and no creation to occur."
    db.database.execute(f"DROP TABLE {str(queue.table)};")
    create_queue_table(queue)
    assert (
        mocked_create.call_count == 1
    ), "Expected table to not exist, triggering create."


def test_using(queue_db):
    """Test that changing option with the "using" method works as expected."""
    store = []

    @queue.task()
    def dummy(id):
        """Dummy task."""
        store.append(id)

    # This job has a priority but fairly low
    dummy.using(expected_at="60s").enqueue(0)
    # Expect this job later, which should delay it's execution (in our case not at all).
    dummy.using(schedule_at="60s").enqueue(1)
    # this job has no priority so it should be executed last
    dummy.enqueue(2)
    # this job has a high priority
    dummy.using(expected_at="0s").enqueue(3)

    queue.work(burst=True)
    assert store == [3, 0, 2], "Unexpected order of completion"


def test_sqla_task(queue_db):
    """Test inserting, deleting and querying tasks using the sqlalchemy integration."""

    store = []

    @queue.task()
    def dummy(*args, **kwargs):
        """Dummy task."""
        r = (args, kwargs)
        store.append(r)

    # insert jobs and check results
    db.database.execute(dummy.sql.insert(1, two=3))
    id_ = db.database.execute(dummy.sql.insert("delete")).fetchone().id
    assert (
        len(db.database.execute(queue.sql.table.select()).fetchall()) == 2
    ), "Expected 2 jobs in the queue"

    # remove job and check results
    db.database.execute(dummy.sql.delete(id_))
    assert (
        len(db.database.execute(queue.sql.table.select()).fetchall()) == 1
    ), "Expected 1 job in the queue after 1 was removed."

    # process jobs and check results
    queue.work(burst=True)
    assert (
        len(db.database.execute(queue.sql.table.select()).fetchall()) == 0
    ), "Expected 0 jobs in the queue after processing jobs."
    assert store == [((1,), {"two": 3})], "Expected one result with passed arguments."


@pytest.mark.parametrize("failures, expected_calls", [(0, 1), (1, 2), (5, 3)])
def test_retry(failures, expected_calls, queue_db, mocker, caplog):

    data = {"calls": 0}
    mocker.patch.object(queue, "_get_timeout").return_value = 0.1

    @queue.task(retry=RetrySpec(max_retries=2, delay_multiplier=0.01))
    def retry_dummy():
        """Dummy task."""

        data["calls"] += 1
        if data["calls"] <= failures:
            raise ValueError("Boom!")

    retry_dummy.enqueue()
    assert len(queue) == 1

    caplog.set_level("INFO")
    queue.work(burst=True)

    assert len(queue) == 0
    assert data["calls"] == expected_calls


def test_work_forever(queue_db, mocker):
    class StopWork(Exception):
        pass

    n_jobs = 3
    n_disconnects = 1
    n_gets = 5

    get_results: list = []
    job_results: list = []

    # Setup mocks to hook into the various stages in our queue loop

    mocked_work = mocker.spy(queue, "work")
    mocked_delay = mocker.patch(
        "app.tasks.sleep", autospec=True
    )  # don't delay when reconnecting

    old_get = queue.get

    def tmp_get(timeout):
        """Mocked version of pulling a new task from the queue.

        We want to stop the test after "n_gets" calls, so we raise a StopWork
        exception to do this.

        Otherwise we just call the original function and add it's result to
        the "get_results" list for later processing.
        """
        assert timeout > 10
        if len(get_results) >= n_gets:
            # Stop the work_forever loop after 3 calls
            raise StopWork()
        result = old_get(timeout=0.1)
        get_results.append(result)
        return result

    mocker.patch.object(queue, "get", side_effect=tmp_get, autospec=True)

    @queue.task(retry=RetrySpec(max_retries=10).dict())
    def dummy(**kwargs):
        """Dummy task.

        Store the passed arguments in "job_results" list.
        In addition, while processing the third job, close the queue connection, which
        will hopefully cause an error which will in turn trigger a
        reconnect.

        """
        job_results.append(kwargs)

        if len(job_results) == 2:
            queue.cursor.close()

        return kwargs

    # Queue a few jobs

    for i in range(n_jobs):
        dummy.enqueue(i=i)

    # Run actual test
    with pytest.raises(StopWork):
        # we don't really work forever since we are raising an error.
        work_forever(queue)

    # Assert that results and calls are as expected

    # Check the number of connects are correct (one at the beginning and one after a disconnect)
    assert (
        mocked_work.call_count == n_disconnects + 1
    ), "Expected a reconnect to have occurred."
    assert (
        mocked_delay.call_count == n_disconnects
    ), "Expected a delay to have been called after disconnect"

    # Check the number of times the actual task function was called
    assert len(job_results) == (n_jobs + n_disconnects), (
        "Expected the number of job calls to be equal to "
        "the number of jobs created + number of disconnects "
        "(refetching job after connection was severed)."
    )
    kwarg_keys = [tuple(r.keys()) for r in job_results]
    assert kwarg_keys == [("i",)] * (n_jobs + n_disconnects)

    # check the number of times an actual job was fetched from the queue
    expected_job_gets = n_jobs + n_disconnects
    expected_empty_gets = len(get_results) - expected_job_gets
    assert [isinstance(r, Job) for r in get_results] == [True] * expected_job_gets + [
        False
    ] * expected_empty_gets
