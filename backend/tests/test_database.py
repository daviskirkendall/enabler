from typing import Set

import sqlalchemy as sa

from app.db import database, metadata

# database test table
table = sa.Table("db_test", metadata, sa.Column("id", sa.Integer, primary_key=True),)


def fetch_ids() -> Set[int]:
    return {r.id for r in database.execute(sa.select([table.c.id])).fetchall()}


def insert_id(id):
    return database.execute(table.insert().values({"id": id}))


def test_database(clean_db):
    """Test that the "Database" object handles connections and transactions as expected."""

    ids = fetch_ids()
    assert set(ids) == set(), "Expected no entries to be present at start."

    con0 = database._connection
    assert (
        con0 and con0.closed is True
    ), "Expected connection to be closed after top level 'execute' call."

    with database.transaction():
        con1 = database._connection
        assert con1, "Expected connection to be set"
        assert con1 is not con0, "Expected new connection to have been established."
        assert con1.closed is False, "Expected connection to not be closed."

        insert_id(1)

        # make sure connection is kept open
        assert (
            database._connection is con1
        ), "Expected connection to be the same within transaction."

        # Check insert worked
        ids = fetch_ids()
        assert set(ids) == {1}, "Expected a single id to have been inserted."

        # handle a nested transaction
        try:
            with database.transaction():
                con2 = database._connection
                assert (
                    con2 is con1
                ), "Expected connection to be held and not regenerated."

                insert_id(2)

                # check contents of database
                ids = fetch_ids()
                assert set(ids) == {1, 2}, "Unexpected results for database results"

                # make sure connection wasn't regenerated
                assert (
                    database._connection is con1
                ), "Expected executions within transactions to not regenerate connection."

                # raise error to trigger rollback
                raise ValueError("boom")
        except Exception:
            pass

        # Make sure connection was kept open
        assert (
            con1.closed is False
        ), "first connection should not have been closed as we are still in a transaction."
        assert (
            database._connection is con1
        ), "Expected connection to be held and not regenerated."

        # Check that contents of the table
        ids = fetch_ids()
        assert set(ids) == {
            1
        }, "Expected the id inserted in failed transaction to have been discarded."

    # check connection was closed
    assert con1.closed, "Expected connection to have been closed after transaction"

    # check database contents
    ids_outer = fetch_ids()
    assert set(ids_outer) == set(ids), "Expected transaction to have been commited"
