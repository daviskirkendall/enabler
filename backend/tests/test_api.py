from fastapi.testclient import TestClient

import app.optimization.tasks
from app.api import app as asgi_app
from app.db import database
from app.models.optimization import optimizations_table
from app.tasks import queue

client = TestClient(asgi_app)


def test_optimizations_api(clean_db, simple_optimization_configuration, queue_db):
    """Test optimization api."""

    response = client.get("/api/optimizations")
    assert response.status_code == 200, response.text
    optimizations = response.json()
    assert len(optimizations) == 0
    assert len(queue) == 0, "Expected to jobs to be in queue."

    # Create a new optimization
    response = client.post(
        "/api/optimizations", json={"configuration": simple_optimization_configuration}
    )
    assert response.status_code == 200, response.text
    result = response.json()
    assert result == {"id": 1}
    assert len(queue) == 1, "Expected queue to be filled with one job."

    # Fetch list of optimizations and ensure that the result is consistent with
    # the single fetch.
    response = client.get("/api/optimizations")
    assert response.status_code == 200, response.text
    optimizations = response.json()
    assert len(optimizations) == 1

    # Fetching optimizations by id should work as expected.
    response = client.get("/api/optimizations/1")
    assert response.status_code == 200, response.text
    result = response.json()
    assert result["id"] == 1
    assert result["status"] == "CREATED"
    assert result["created_at"]
    assert result["updated_at"]
    assert (
        result["updated_at"] == result["created_at"]
    ), "Creation and update should be the same."
    assert result["finished_at"] is None
    assert (
        optimizations[0] == result
    ), "Expected the result by id to be the same as through the list api."


def test_optimizations_api_roundtrip(
    clean_db, simple_optimization_configuration, queue_db, mocker
):
    """Test optimization api."""
    optimization_task = mocker.spy(app.optimization.tasks.run_optimization, "f")

    assert len(queue) == 0, "Expected queue to be empty at start."

    response = client.post(
        "/api/optimizations", json={"configuration": simple_optimization_configuration}
    )
    assert response.status_code == 200, response.text
    result = response.json()
    assert result == {"id": 1}
    assert len(queue) == 1, "Expected queue to be filled with one job."

    # process all tasks in queue
    queue.work(burst=True)

    # Assure everything has been called
    assert (
        optimization_task.call_count == 1
    ), 'Expected task to have been called once.  Check the tests in "test_tasks.py" to be sure everything works.'

    assert len(queue) == 0, "Expected all jobs to have been processed"

    # Fetch the optimization and ensure that all fields are set as expected.
    response = client.get("/api/optimizations/1")
    assert response.status_code == 200, response.text
    result = response.json()
    assert result["id"] == 1
    assert result["status"] == "SUCCESS"

    for field in ["created_at", "updated_at", "finished_at"]:
        assert result[field], f"Expected field {field} to be non null."

    assert (
        result["finished_at"] > result["created_at"]
    ), "Expected finished to be after created."
    assert (
        result["finished_at"] == result["updated_at"]
    ), "Finished and update should be the same (the last update should have happened upon finishing)."

    # Make sure the result was saved as well
    response = client.get("/api/optimizations/1/result")
    assert response.status_code == 200
    result = response.json()
    assert result["objective"], "Expected the objective field to hold an actual value."
    assert result["allocations"]
    assert (
        len(result["allocations"]) > 10
    ), "Expected a non-trivial number of allocations to be present"
    assert all(result["allocations"]), "Expected all allocations to be filled."


def test_optimization_result_api(clean_db):
    result_data = {"test": "result"}

    # Insert result into table
    id = database.execute(
        optimizations_table.insert()
        .values(result=result_data, configuration={})
        .returning(optimizations_table.c.id)
    ).first()["id"]

    # query api and assert result
    response = client.get(f"/api/optimizations/{id}/result")
    assert response.status_code == 200, response.text
    assert response.json() == result_data


def test_optimization_configuration_api(simple_optimization_configuration, clean_db):
    # Insert result into table
    id = database.execute(
        optimizations_table.insert()
        .values(configuration=simple_optimization_configuration)
        .returning(optimizations_table.c.id)
    ).first()["id"]

    # query api and assert result
    response = client.get(f"/api/optimizations/{id}/configuration")
    assert response.status_code == 200, response.text
    assert response.json() == simple_optimization_configuration


def test_optimization_nonexistent_configuration_api(
    simple_optimization_configuration, clean_db
):
    # query api and assert result
    response = client.get("/api/optimizations/1/configuration")
    assert response.status_code == 404


def test_optimization_nonexistent_result_api(
    simple_optimization_configuration, clean_db
):
    # Insert into table (but without result)
    id = database.execute(
        optimizations_table.insert()
        .values(configuration={})
        .returning(optimizations_table.c.id)
    ).first()["id"]

    # query api and assert result is not found
    response = client.get(f"/api/optimizations/{id}/result")
    assert response.status_code == 200
    assert response.json() is None
