import datetime
import json
from typing import Any

import pytest
import requests
from _pytest.mark import param

from app.sources.divi import archive
from app.sources.divi.archive import (
    DiviArchivePullError,
    fill_archive_db,
    from_archive_db,
    pull_divi_archive_csv_by_date,
)

csv_format_1 = """
bundesland,kreis,anzahl_standorte,betten_frei,betten_belegt,faelle_covid_aktuell_im_bundesland
01,01001,5,20,30,28
01,01002,4,30,40,28
""".strip()

csv_format_2 = """
bundesland,gemeindeschluessel,anzahl_meldebereiche,faelle_covid_aktuell,faelle_covid_aktuell_beatmet,anzahl_standorte,betten_frei,betten_belegt,daten_stand
01,01001,1,0,0,2,20,30,2020-07-04 12:15:00
01,01002,2,0,0,3,30,40,2020-07-04 12:15:00
""".strip()


def build_response(status_code=200, content: Any = "", headers: dict = None):
    response = requests.Response()

    response.status_code = status_code

    # convert content to bytes
    if not isinstance(content, bytes):
        if not isinstance(content, str):
            content = json.dumps(content)
        content = bytes(content, "utf-8")
    response._content = content  # type: ignore

    response.headers = requests.utils.CaseInsensitiveDict(headers)

    return response


def build_url(*args):
    return archive.build_divi_csv_archive_url(datetime.datetime(*args), 0)


def mock_requests_get(mocker, responses):
    def tmp_get(url, timeout=None):
        assert timeout is not None, "We always want a timeout set!"
        try:
            response = responses[url]
        except KeyError:
            response = build_response(404)

        # save the data for debugging purposes
        response._responses = responses
        response._url = url

        return response

    return mocker.patch.object(requests, "get", side_effect=tmp_get)


@pytest.fixture
def csv_cache_folder(tmp_path, monkeypatch):
    """Replace csv cache."""
    csv_cache_folder = tmp_path / "divi" / "csv"
    csv_cache_folder.mkdir(parents=True, exist_ok=True)
    monkeypatch.setattr(archive.csv_cache, "cache_folder", csv_cache_folder)
    return csv_cache_folder


@pytest.mark.parametrize(
    "responses,expected",
    [
        param(
            {
                build_url(2020, 4, 24, 12, 15): build_response(
                    200, 'has ".../download"'
                ),
                build_url(2020, 4, 24, 12, 15)
                + "/download": build_response(200, "yeah!"),
            },
            "yeah!",
            id="success on first try",
        ),
        param(
            {
                build_url(2020, 4, 24, 9, 15): build_response(
                    200, 'has ".../download"'
                ),
                build_url(2020, 4, 24, 9, 15)
                + "/download": build_response(200, "yeah!"),
            },
            "yeah!",
            id="success on second try (12:15)",
        ),
        param(
            {build_url(2020, 4, 24, 9, 15): build_response(200, 'has ".../download"')},
            DiviArchivePullError,
            id="csv file not found.",
        ),
        param(
            {
                build_url(2020, 4, 24, 9, 15): build_response(
                    200, 'has ".../download"'
                ),
                build_url(2020, 4, 24, 9, 15)
                + "/download": build_response(
                    200, "yeah!", {"Content-Type": "text/html"}
                ),
            },
            DiviArchivePullError,
            id="csv url return html page.",
        ),
    ],
)
def test_pull_divi_archive_csv_by_date(
    mocker, monkeypatch, csv_cache_folder, responses, expected
):
    mock_requests_get(mocker, responses)
    if isinstance(expected, type) and issubclass(expected, Exception):
        with pytest.raises(expected):
            pull_divi_archive_csv_by_date(datetime.date(2020, 4, 24))
        assert not (csv_cache_folder / "2020-04-24.csv").exists()
    else:
        actual = pull_divi_archive_csv_by_date(datetime.date(2020, 4, 24))
        assert actual == expected
        assert (csv_cache_folder / "2020-04-24.csv").exists()


def test_fill_archive_db(csv_cache_folder, mocker, clean_db):
    responses = {
        build_url(2020, 4, 24, 12, 15): build_response(200, 'has ".../download"'),
        build_url(2020, 4, 24, 12, 15) + "/download": build_response(200, csv_format_1),
        build_url(2020, 4, 25, 12, 15): build_response(200, 'has ".../download"'),
        build_url(2020, 4, 25, 12, 15) + "/download": build_response(200, csv_format_2),
    }
    mock_requests_get(mocker, responses)
    fill_archive_db(start=datetime.date(2020, 4, 24), end=datetime.date(2020, 4, 25))
    results = from_archive_db()
    expected_entries = 4  # each row of the mocked csv files
    assert len(results) == expected_entries, "Unexpected number of entries"


@pytest.mark.webtest
def test_fetch_hospital_locations():
    result = archive.fetch_hospital_locations()
    assert result


@pytest.mark.webtest
def test_fetch_hospital_status_from_api():
    result = archive.fetch_hospital_status_from_api()
    assert result
