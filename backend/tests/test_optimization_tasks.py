import sqlalchemy as sa

from app.db import database
from app.models.optimization import optimizations_table
from app.optimization.tasks import create_new_optimization, run_optimization
from app.schemas.optimization import ModelResults
from app.tasks import queue


def test_task_run_optimization(
    mocker, simple_optimization_configuration, queue_db, clean_db
):
    patched_model_cls = mocker.patch("app.optimization.tasks.MatchingModel")
    mock_results = ModelResults(objective=1, occupation={}, allocations=[])
    patched_model_cls.return_value.results = mock_results

    # Create and run optimization
    id = create_new_optimization(simple_optimization_configuration)
    run_optimization.enqueue(id)

    queue.work(burst=True)

    # Check results
    assert patched_model_cls.call_count == 1

    result = database.execute(optimizations_table.select()).fetchall()

    assert len(result) == 1
    entry = result[0]
    assert entry.result == mock_results.dict()
    assert entry.status == "SUCCESS"
    non_empty_fields = {k for k, v in entry.items() if v is not None}
    assert non_empty_fields == {
        "id",
        "configuration",
        "created_at",
        "updated_at",
        "started_at",
        "finished_at",
        "result",
        "job_queue_id",
        "status",
    }


def test_task_run_optimization_fail(
    mocker, simple_optimization_configuration, queue_db, clean_db
):
    patched_model_cls = mocker.patch("app.optimization.tasks.MatchingModel")
    patched_model_cls.side_effect = ValueError("Boom!")

    # Create and run optimization
    id = create_new_optimization(simple_optimization_configuration)
    run_optimization.enqueue(id)

    queue.work(burst=True)

    # Check results
    assert patched_model_cls.call_count == 1

    result = database.execute(optimizations_table.select()).fetchall()

    assert len(result) == 1
    entry = result[0]
    assert entry.status == "FAILURE"
    assert entry.error == {"type": "ValueError", "msg": "Boom!"}
    non_empty_fields = {k for k, v in entry.items() if v is not None}
    assert non_empty_fields == {
        "id",
        "configuration",
        "created_at",
        "updated_at",
        "started_at",
        "finished_at",
        "error",
        "job_queue_id",
        "status",
    }


def test_task_run_optimization_db_fail(
    mocker, simple_optimization_configuration, queue_db, clean_db
):
    patched_model_cls = mocker.patch("app.optimization.tasks.MatchingModel")
    mock_results = ModelResults(objective=1, occupation={}, allocations=[])
    patched_model_cls.return_value.results = mock_results
    broken = False

    def break_db():
        nonlocal broken
        if not broken:
            broken = True
            raise sa.exc.OperationalError(None, None, Exception())

    patched_model_cls.return_value.solve.side_effect = break_db

    result = database.execute(optimizations_table.select()).fetchall()
    assert len(result) == 0, "No optimizations should be present"

    # Create and run optimization
    id = create_new_optimization(simple_optimization_configuration)
    run_optimization.enqueue(id)

    queue.work(burst=True)

    # Check results
    assert (
        patched_model_cls.call_count == 2
    ), "Model should have been constructed twice (because of one retry)."

    result = database.execute(optimizations_table.select()).fetchall()

    assert len(result) == 1
    entry = result[0]
    assert entry.status == "SUCCESS"
    assert entry.result == mock_results.dict()
