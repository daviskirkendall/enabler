"""Test germany sources.

Note that many of these tests require web access.
"""
import pytest
import sqlalchemy as sa

from app.db import database
from app.models.germany import german_communities_table, german_districts_table
from app.sources import germany


@pytest.fixture(scope="module")
def downloaded_excel_file():
    """Download community/district excel file to temporary cache folder for tests in the module."""
    yield germany.download_excel_file()


@pytest.fixture
def tmp_cache_folder(tmp_path, monkeypatch):
    """Tmporarily change cache folder."""
    tmp_cache_folder = tmp_path / "germany_cache"
    monkeypatch.setattr(germany, "CACHE_FOLDER", tmp_cache_folder)
    return tmp_cache_folder


@pytest.mark.webtest
def test_download_excel_file(tmp_cache_folder):
    """Download community/district excel file to temporary cache folder.

    Note that this test requires network access.
    """
    assert not tmp_cache_folder.exists()
    filename = germany.download_excel_file()
    assert tmp_cache_folder.exists()
    assert (
        filename.parent == tmp_cache_folder
    ), "expected file to be stored in cache folder."
    assert filename.exists(), "expected downloaded file to exist."
    assert filename.stat().st_size > 1000000, "Expected file size to be larger than 1MB"


@pytest.mark.webtest
def test_communities_from_excel_file(downloaded_excel_file):
    """Test parsing excel file and reading out commuities.

    Note that this test requires network access (if excel file has not been downloaded yet).
    """
    communities = germany.iter_communities_from_excel_file(downloaded_excel_file)
    community = next(communities)
    assert community == germany.schemas.Community(
        name="Flensburg, Stadt",
        community_id="010010000000",
        district_id="01001",
        state_id="01",
        population=44599,
        area=56,
        latitude=54.78252,
        longitude=9.43751,
        zip_code="24937",
    )


@pytest.mark.webtest
def test_district_from_excel_file(downloaded_excel_file):
    """Test parsing excel file and reading out districts.

    Note that this test requires network access (if excel file has not been downloaded yet).
    """
    districts = germany.iter_districts_from_excel_file(downloaded_excel_file)
    district = next(districts)
    assert district == germany.schemas.District(
        district_id="01001", name="Flensburg, Stadt", state_id="01"
    )


def test_aggregate_district():
    """Test aggregating district data from communities works as expected.

    Area, population and latitude/longitude should be computed from the
    communities located in each district.
    """
    communities = [
        germany.schemas.Community(
            name="TestC1",
            community_id="010010000000",
            population=4000,
            area=2,
            latitude=50.0,
            longitude=10.0,
        ),
        germany.schemas.Community(
            name="TestC1",
            community_id="010019999999",
            population=1000,
            area=10,
            latitude=55.0,
            longitude=8.0,
        ),
        germany.schemas.Community(
            name="Standalone",
            community_id="020019999999",
            population=2000,
            area=10,
            latitude=53.0,
            longitude=9.0,
        ),
    ]

    districts = [
        germany.schemas.District(name="district1", district_id="01001"),
        germany.schemas.District(name="ignored", district_id="07001"),
    ]

    expected = [
        germany.schemas.AggregatedDistrict(
            name="district1",
            district_id="01001",
            state_id="01",
            population=5000,
            area=12,
            latitude=51,
            longitude=9.6,
        ),
        germany.schemas.AggregatedDistrict(
            name="Unknown (02001)",
            district_id="02001",
            state_id="02",
            population=2000,
            area=10,
            latitude=53.0,
            longitude=9.0,
        ),
    ]

    actual = list(germany.iter_aggregate_districts(districts, communities))
    assert actual == expected


@pytest.mark.webtest
def test_integration(clean_db):
    """Run integration test.

    * Download (or read from cache) excel
    * Parse districts/communities from excel
    * Fill database
    * Fetch from database
    * Modify database
    * Refill database
    * Check that old and new database states are equivilant

    Note that this test takes quite a long time to run and may require network access.
    """
    germany.fill_db()

    districts = list(germany.districts_from_db())
    communities = list(germany.communities_from_db())

    assert districts
    assert communities

    assert 1000 > len(districts) > 100, "Number of districts out of range"
    assert 100000 > len(communities) > 10000, "Number of communities out of range"

    assert isinstance(districts[-1], germany.schemas.AggregatedDistrict)
    assert isinstance(communities[-1], germany.schemas.Community)

    with database.transaction():
        table = german_districts_table
        id = table.c.district_id
        stmt = table.delete().where(id.in_(sa.select([id]).limit(10)))
        database.execute(stmt)

        table = german_communities_table
        id = table.c.community_id
        stmt = table.delete().where(id.in_(sa.select([id]).limit(10)))
        database.execute(stmt)

    len(list(germany.districts_from_db())) == len(districts) - 10
    len(list(germany.communities_from_db())) == len(communities) - 20

    germany.fill_db()

    districts2 = list(germany.districts_from_db())
    assert len(districts2) == len(districts)
    assert {c.district_id for c in districts} == {c.district_id for c in districts2}

    communities2 = list(germany.communities_from_db())
    assert len(communities2) == len(communities)
    assert {c.community_id for c in communities} == {
        c.community_id for c in communities2
    }
