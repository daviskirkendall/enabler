"""Script to create an example scenario."""
import datetime
import json
import logging
from dataclasses import dataclass
from functools import cached_property
from pathlib import Path
from typing import List, NamedTuple, Set

import numpy as np
import pandas as pd
from PIL import Image
from plotly import graph_objects as go

from app.schemas.optimization import ModelConfiguration, ModelResults
from app.sources.divi.archive import from_archive_db
from app.sources.germany import districts_from_db
from app.sources.rki.fetcher import daily_cases_from_db

logger = logging.getLogger(__name__)


@dataclass
class InputSettings:
    """Simple schema for storing input settings for configuration creation."""

    start: datetime.date
    end: datetime.date
    df_covid: pd.DataFrame
    df_divi_mean: pd.DataFrame
    df_districts: pd.DataFrame
    locations: Set[str]

    @property
    def days(self):
        return (self.end - self.start).days


def input_settings(
    start_date: datetime.date, end_date: datetime.date, state_id: str = None,
) -> InputSettings:
    """Generate input settings for creating a configuration.

    Pulls regional (germany), covid (rki) and intensive care (divi) data.
    """
    logger.info("Importing data...")
    # Import covid cases
    covid_cases = daily_cases_from_db(state_id=state_id)
    df_covid = pd.DataFrame(c.dict() for c in covid_cases)
    # Berlin has multiple districts in the covid dataset for some reason
    df_covid = _fix_berlin_covid_cases(df_covid)
    df_covid["date"] = pd.to_datetime(df_covid["date"])
    df_covid = df_covid.sort_values("date")

    # Import hospital capacities
    divi_records = from_archive_db(state_id=state_id)
    df_divi = pd.DataFrame([r.dict() for r in divi_records])

    # Import location information
    districts = districts_from_db(state_id=state_id)
    df_districts = pd.DataFrame(d.dict() for d in districts)
    df_districts = df_districts.set_index("district_id")

    logger.info("Calculating input settings...")

    # Calculate average occupation and capacity for each district
    df_divi_mean = df_divi.groupby("district_id").mean()
    df_divi_mean["capacity"] = round(
        df_divi_mean["bed_unoccupied_count"] + df_divi_mean["bed_occupied_count"]
    )
    df_divi_mean["occupation"] = round(df_divi_mean["bed_occupied_count"])

    # Get locations with enough data in divi and rki datasets
    locations = set(df_divi["district_id"]) & set(df_covid["district_id"])

    # Return variables
    return InputSettings(
        start=start_date,
        end=end_date,
        df_covid=df_covid,
        df_divi_mean=df_divi_mean,
        df_districts=df_districts,
        locations=locations,
    )


def create_covid_forecast(
    start: datetime.date,
    end: datetime.date,
    location: str,
    df_covid: pd.DataFrame,
    intensive_covid_fraction: float,
    time_covid_to_intensive: int,
    covid_case_factor: float = 1.0,
) -> List[int]:
    """Create a forecast of covid intensive care patients.

    We just observe the number of cases that were reported days before the date (time_covid_to_intensive).
    In addition we assume a fraction of the cases to become intensive care patients (intensive_covid_fraction).
    """
    # time it takes for a covid patient to show up in intensive care
    time_covid_to_intensive = pd.to_timedelta(time_covid_to_intensive, unit="days")
    timeindex = pd.date_range(start, end, freq="d") - time_covid_to_intensive

    cases = pd.DataFrame(
        df_covid.loc[df_covid["district_id"] == location][["date", "cases"]]
    )
    cases = cases.set_index("date").reindex(timeindex).fillna(0)

    # Calculate number of intensive care patients from covid cases.
    # We also need to ensure that the number of cases are always integer values
    # so the optimization engine can handle them.
    cases = round(cases["cases"] * intensive_covid_fraction * covid_case_factor)
    return cases.values.tolist()


def create_usual_forecast(
    location: str, df_divi_mean: pd.DataFrame, discharge_days: int, days: int
) -> List[int]:
    """Create forecast for "usual" intensive care patients.

    This should be an approximation for the baseline occupation of intensive
    care beds when no spikes due to a pandemic are occuring.

    In this case the forecast is extremely simple and just assumes that every
    day new patients come in such that the average occupation for a location
    is maintained (consant forecast over all days).
    """
    avg_discharge = df_divi_mean.loc[location].occupation / discharge_days
    return [int(avg_discharge)] * days


def create_discharge_forecast(
    location: str, df_divi_mean: pd.DataFrame, discharge_days: int, days: int
) -> List[int]:
    """Create forecast for how many intensive care patients are discharged each day.

    The discharge forecast calculated here is simple as it assumes a constant
    number of days that a patient is in intensive care (`discharge_days`).
    """
    avg_discharge = df_divi_mean.loc[location].occupation // discharge_days
    remainder = df_divi_mean.loc[location].occupation % discharge_days
    forecast = np.zeros(days)
    forecast[:discharge_days] = avg_discharge
    forecast[discharge_days] = remainder
    return forecast.tolist()


def create_config_file(
    filename: str,
    start_date: datetime.date,
    end_date: datetime.date,
    discharge_days: int,
    usual_avg_healing_time: int,
    covid_avg_healing_time: int,
    time_covid_to_intensive: int,
    intensive_covid_fraction: float,
    covid_case_factor: float,
    state_id: str = None,
) -> None:
    """Creates an optimization configuration and saves it to the given filename."""
    settings = input_settings(
        start_date=start_date, end_date=end_date, state_id=state_id,
    )
    configuration = {
        "forecast_horizon": settings.days,
        "locations": [],
        "meta": {
            "start_date": start_date,
            "end_date": end_date,
            "discharge_days": discharge_days,
            "usual_avg_healing_time": usual_avg_healing_time,
            "covid_avg_healing_time": covid_avg_healing_time,
            "time_covid_to_intensive": time_covid_to_intensive,
            "intensive_covid_fraction": intensive_covid_fraction,
            "covid_case_factor": covid_case_factor,
            "state_id": state_id,
        },
    }

    logger.info("Build locations...")
    for location in settings.locations:
        covid_forecast = create_covid_forecast(
            start=settings.start,
            end=settings.end,
            location=location,
            df_covid=settings.df_covid,
            intensive_covid_fraction=intensive_covid_fraction,
            time_covid_to_intensive=time_covid_to_intensive,
            covid_case_factor=covid_case_factor,
        )
        usual_forecast = create_usual_forecast(
            location, settings.df_divi_mean, discharge_days, settings.days
        )
        discharge_forecast = create_discharge_forecast(
            location, settings.df_divi_mean, discharge_days, settings.days
        )
        location_entry = {
            "name": f"{location} {settings.df_districts.loc[location]['name']}",
            "longitude": settings.df_districts.loc[location].longitude,
            "latitude": settings.df_districts.loc[location].latitude,
            "occupation": settings.df_divi_mean.loc[location].occupation,
            "capacity": settings.df_divi_mean.loc[location].capacity,
            "discharge_forecast": discharge_forecast,
            "new_patients": [
                {
                    "category": "usual",
                    "forecast": usual_forecast,
                    "avg_healing_time": usual_avg_healing_time,
                    "relocation_possible": False,
                },
                {
                    "category": "covid",
                    "forecast": covid_forecast,
                    "avg_healing_time": covid_avg_healing_time,
                    "relocation_possible": True,
                },
            ],
        }
        configuration["locations"].append(location_entry)
    final_config = ModelConfiguration.parse_obj(configuration)
    with open(filename, "w") as f:
        f.write(final_config.json(indent=4, ensure_ascii=False))

    logger.info(f"Wrote configuration to file: {filename}")


# Plotting


class ResultsStore(NamedTuple):
    """Simple container for contents of an optimization results file."""

    configuration: ModelConfiguration
    results: ModelResults
    max_distance: int


class Plotter:
    def __init__(
        self,
        filename: str = "examples/results_config.json",
        geojson_filename: str = "data/landkreise_simplify200.geojson",
    ) -> None:
        self.configuration, self.results, self.max_distance = _read_results(filename)
        self.districts_geojson = _load_geojson(geojson_filename)

    @cached_property
    def district_df(self) -> pd.DataFrame:
        districts = [d.dict() for d in districts_from_db()]
        df = pd.DataFrame(districts).set_index("district_id")
        return df

    @cached_property
    def allocations_df(self) -> pd.DataFrame:
        df = pd.DataFrame(a.dict() for a in self.results.allocations)
        df["from_location"] = df["from_location"].apply(lambda s: s[:5])
        df["to_location"] = df["to_location"].apply(lambda s: s[:5])
        df = df[df["from_location"] != df["to_location"]]
        df = (
            df.groupby(["from_location", "to_location", "time"])["result"]
            .sum()
            .sort_index()
            .reset_index()
        )
        self._add_geo_to_df(df, "from_location", "from_")
        self._add_geo_to_df(df, "to_location", "to_")
        return df.set_index("time")

    @cached_property
    def occupation_df(self) -> pd.DataFrame:
        df = pd.DataFrame(
            {"location": loc[:5], "result": result, "time": time}
            for loc, occ in self.results.occupation.items()
            for time, result in enumerate(occ)
        )
        self._add_geo_to_df(df, "location")
        df = df.set_index(["time", "location"]).sort_index()
        return df

    @cached_property
    def location_df(self) -> pd.DataFrame:
        # load locations from configuration
        df = pd.DataFrame(
            {
                "longitude": loc.longitude,
                "latitude": loc.latitude,
                "name": loc.name,
                "location": loc.name[:5],
                "capacity": loc.capacity,
            }
            for loc in self.configuration.locations
        ).set_index("location", drop=False)
        return df

    @cached_property
    def plot_data_df(self) -> pd.DataFrame:
        """Combine locations and occupations and prepare for plotting."""
        df = self.occupation_df.rename({"result": "occupation"}, axis=1)
        df = df[["occupation"]]
        # fill all districts, not just ones that have allocations
        new_index = pd.MultiIndex.from_product(
            [df.index.levels[0], self.district_df.index]
        )
        df = df.reindex(new_index, fill_value=0)
        # Calculate additional columns
        location_values = df.index.get_level_values(1)
        df["capacity"] = location_values.map(self.location_df["capacity"])
        df["district_name"] = location_values.map(self.location_df["name"])
        df["usage_percent"] = (df["occupation"] / df["capacity"]).fillna(0) * 100
        df["usage_percent_100_max"] = df["usage_percent"].clip(0, 150).round(1)
        df["real_occupation"] = df[["occupation", "capacity"]].min(axis=1)
        df["over_occupation"] = df["occupation"] - df["real_occupation"]
        return df

    @cached_property
    def aggregate_forecast_df(self) -> pd.DataFrame:
        """Collect forecasts for patients and aggregate them."""
        df = pd.DataFrame()
        for location in self.configuration.locations:
            for patient_forecast in location.new_patients:
                if patient_forecast.category in list(df.columns):
                    df[patient_forecast.category] += patient_forecast.forecast[
                        : self.configuration.forecast_horizon
                    ]
                else:
                    df[patient_forecast.category] = patient_forecast.forecast[
                        : self.configuration.forecast_horizon
                    ]
        return df

    @cached_property
    def times(self) -> List[int]:
        """Calculate times that are present."""
        return sorted(self.plot_data_df.index.levels[0].unique())

    @cached_property
    def datetime_times(self) -> pd.DatetimeIndex:
        """Get datetime index."""
        return pd.date_range(
            start=self.configuration.meta["start_date"],
            end=self.configuration.meta["end_date"],
            freq="d",
        )[:-1]

    @cached_property
    def max_relocated_patients(self):
        """Calculate maximum relocated patients."""
        return float(self.source_df["result"].max())

    @cached_property
    def source_df(self) -> pd.DataFrame:
        """Relocations grouped by from_location (and time)."""
        return (
            self.allocations_df.reset_index()
            .groupby(["time", "from_location"])
            .agg({"result": "sum", "from_latitude": "first", "from_longitude": "first"})
            .reset_index(level="from_location")
        )

    def get_figure(self, t: int) -> go.Figure:
        date = self.datetime_times[t].date()
        print(f"generating figure for t = {t}, {date}")
        fig = go.Figure()
        plot_df = self.plot_data_df.loc[t]
        choro = go.Choroplethmapbox(
            z=plot_df["usage_percent_100_max"],
            zmin=40,
            zmax=160,
            locations=plot_df.index,
            geojson=self.districts_geojson,
            featureidkey="properties.AGS",
            colorscale="YlOrRd",
            colorbar=dict(
                title="Auslastung [%]", thickness=16, outlinewidth=0, len=0.8
            ),
            text=plot_df.apply(
                lambda r: f"{r['district_name']} ({r.name}), Auslastung {round(r['usage_percent_100_max'])} %",
                axis="columns",
            ),
            name="",
        )
        fig.add_trace(choro)

        allocations_t_df = pd.DataFrame()
        if t in self.allocations_df.index.tolist():
            allocations_t_df = self.allocations_df.loc[t]
            if isinstance(allocations_t_df, pd.Series):
                allocations_t_df = pd.DataFrame(allocations_t_df).transpose()

        if len(allocations_t_df):
            source_t_df = self.source_df.loc[t]
            if isinstance(source_t_df, pd.Series):
                source_t_df = pd.DataFrame(source_t_df).transpose()

            fig.add_trace(
                go.Scattermapbox(
                    lon=source_t_df["from_longitude"],
                    lat=source_t_df["from_latitude"],
                    mode="markers",
                    showlegend=False,
                    marker={
                        "color": "green",
                        "size": (
                            10
                            + (
                                15.0
                                * (source_t_df["result"] / self.max_relocated_patients)
                            )
                        ).tolist(),
                    },
                    text=[f"verlegt: {r}" for r in source_t_df["result"]],
                    hoverinfo="text",
                    opacity=0.8,
                    name="",
                )
            )

            for _, row in allocations_t_df.iterrows():
                fig.add_trace(
                    go.Scattermapbox(
                        lon=[row["from_longitude"], row["to_longitude"]],
                        lat=[row["from_latitude"], row["to_latitude"]],
                        mode="lines",
                        line=dict(width=row["result"], color="green"),
                        opacity=0.8,
                        showlegend=False,
                        text=f"{row['result']} von {row['from_location']} nach {row['to_location']} verlegt",
                        hoverinfo="text",
                        name="",
                    )
                )
        if self.max_distance == 0:
            title = f"{date} Auslastung der Intensivstationen ohne Koordination"
        else:
            title = f"{date} Auslastung der Intensivstationen mit Koordination"
        fig.update_layout(
            mapbox_style="white-bg",  # "carto-positron",
            mapbox_zoom=5.2,
            mapbox_center={"lat": 51.2, "lon": 10.4},
            title=title,
            margin={"r": 0, "t": 50, "l": 0, "b": 0},
            width=600,
            height=700,
        )
        return fig

    def save_map_plots(
        self, dirname: str = "examples/plots/days", pattern="day_{:0>3d}.png"
    ) -> List[Path]:
        """Save map plots for all days."""
        root = Path(dirname)
        root.mkdir(parents=True, exist_ok=True)
        filenames = []
        logger.info(f"Creating and writing {len(self.times)} images to {root/pattern}")
        for t in self.times:
            filename = root / pattern.format(t)
            fig = self.get_figure(t)
            fig.write_image(str(filename), width=600, height=700)
            logger.info(f"Wrote image for time {t} to {filename}")
            filenames.append(filename)
        return filenames

    def save_timeseries_plot(self, filename):
        """Plot timeseries and save it to a file."""
        df_sum = self.plot_data_df.groupby("time").sum()
        df_forecasts = self.aggregate_forecast_df
        fig = go.Figure()
        fig.add_trace(
            go.Scatter(
                x=self.datetime_times,
                y=df_sum["over_occupation"],
                mode="lines",
                name="Intensivpatienten ohne Versorgung",
            )
        )
        fig.add_trace(
            go.Scatter(
                x=self.datetime_times,
                y=df_forecasts["covid"],
                mode="lines",
                name="Covid Patienten mit Intensivbedarf",
            )
        )
        fig.write_image(str(filename), width=1200, height=600)

    @staticmethod
    def create_gif(
        filename: str = "examples/plots/plot.gif",
        source_dirname: str = "examples/plots/days",
    ):
        """Create a gif from all png files in `source_dirname` and save it to `filename`."""

        # https://pillow.readthedocs.io/en/stable/handbook/image-file-formats.html#gif
        img, *imgs = [Image.open(f) for f in sorted(Path(source_dirname).glob("*.png"))]
        img.save(
            fp=filename,
            format="GIF",
            append_images=imgs,
            save_all=True,
            duration=2000,
            loop=0,
        )

    def _add_geo_to_df(
        self, df: pd.DataFrame, district_id_column: str, prefix: str = ""
    ) -> None:
        """Add latitude and longitude columns to dataframe."""
        df[prefix + "longitude"] = df[district_id_column].map(
            self.district_df["longitude"]
        )
        df[prefix + "latitude"] = df[district_id_column].map(
            self.district_df["latitude"]
        )


def _load_geojson(filename: str = "data/landkreise_simplify200.geojson") -> dict:
    """Load geojson data.

    The default file was generated by visiting:
    http://opendatalab.de/projects/geojson-utilities/
    select all kreise and lowest quality.
    """
    with open(filename) as f:
        districts_geojson = json.load(f)
    return districts_geojson


def _read_results(filename: str) -> ResultsStore:
    """Read optimization results file."""
    with open(filename) as f:
        data = json.load(f)
    results = ModelResults.parse_obj(data["results"])
    configuration = ModelConfiguration.parse_obj(data["configuration"])
    max_distance = data["max_distance"]
    return ResultsStore(
        configuration=configuration, results=results, max_distance=max_distance
    )


def _fix_berlin_covid_cases(df_covid: pd.DataFrame) -> pd.DataFrame:
    """Aggregate all berlin districts together.

    RKI collects data from districts within berlin that are not counted as separate districts elsewhere.
    We aggregate all berlin districts to one district as is done in the official german government data.
    """
    df_covid_berlin = df_covid[df_covid["state_id"] == "11"]
    df_covid_berlin["district_id"] == "11000"
    df_covid_berlin.groupby("date", as_index=False).agg(
        {"cases": "sum", "district_id": lambda s: "11000", "state_id": "first"}
    )
    df_covid_berlin = df_covid.loc[df_covid["state_id"] == "11", "district_id"]
    return pd.concat([df_covid[df_covid["state_id"] != "11"], df_covid_berlin], axis=0)
