"""Setup file for constrainer."""
import os

from setuptools import setup


def read(fname):
    """Utility function to read the README file.
    Used for the long_description.  It's nice, because now 1) we have a top level
    README file and 2) it's easier to type in the README file than to put a raw
    string in below ...
    """
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


tests_require = ["pytest", "coverage", "pytest-mock"]

check_requires = ["black", "isort", "flake8", "mypy"]

dev_requires = ["hupper", "pdbpp"] + check_requires + tests_require

setup(
    name="enabler",
    version="0.0.1",
    author="Davis Kirkendall",
    author_email="davis.e.kirkendall@gmail.com",
    description="Library to build constraint programming models and solve them.",
    packages=["app"],
    long_description=read("README.md"),
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Healthcare Industry",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.8",
    ],
    extras_require={
        "dev": dev_requires,
        "check": check_requires,
        "test": tests_require,
    },
    tests_require=tests_require,
    entry_points={"console_scripts": "enabler=app.cli:cli"},
)
